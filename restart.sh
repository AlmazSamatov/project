sudo kill -9 `sudo lsof -t -i:8080`
sudo kill -9 `sudo lsof -t -i:3000`
pkill -f "telegram_bot"
cd dds-project-backend
mvn package
cp application.properties target/application.properties
cd target
java -Dspring.config.location=application.properties -jar dds-project-backend-1.0.jar &>> logs.txt & disown
cd ../../telegram-bot
pip3 install -r requirements.txt
python3 telegram_bot.py & disown
cd ../front
npm install
npm start & disown
cd ..