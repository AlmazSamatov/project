#!/usr/bin/env python
# -*- coding: utf-8 -*-
import telebot
import requests
from tinydb import TinyDB, Query
from base64 import b64encode
import pyqrcode
import io

token = '1048721945:AAGXvf4wSbSd_Bw09hgpjjjZzR3tijDOxhE'
server_address = '134.209.252.11'


tg_bot_token = "@aMcY[?)!tyLc'3_e]5SNG)Gj=q6ZB_4rhSSk`xeTwYQ(xgx>$JZ):e5KjK{"
bot = telebot.TeleBot(token)
db = TinyDB('/root/project/telegram-bot/db.json')
server_address = 'http://{}:8080'.format(server_address)


def save_to_db(username, auth_token, user_id):
	db.insert({'username': username, 'auth_token': auth_token, 'user_id': user_id})

def get_from_db(username):
	User = Query()
	res = db.search(User.username == username)[0]
	return res

def is_in_db(username):
	User = Query()
	res = db.search(User.username == username)
	return len(res) != 0

def register(username):
	user_data = {
		'name': username,
		'telegram_alias': username,
		'telegram_bot_token': tg_bot_token
	}
	r = requests.post(server_address + '/v1/telegram-bot/registration', json=user_data)
	return r
	
def login(username):
	user_data = {
		'telegram_alias': username,
		'telegram_bot_token': tg_bot_token
	}
	r = requests.post(server_address + '/v1/telegram-bot/login', json=user_data)
	return r

def get_groups(username):
	r = requests.get(server_address + '/v1/user/groups', headers={"Authorization": get_from_db(username)['auth_token']})
	return r

def get_groups_to_join(username):
	r = requests.get(server_address + '/v1/groups', headers={"Authorization": get_from_db(username)['auth_token']})
	return r

def join_group(username, group_id):
	group_data = {
		'group_id': group_id
	}
	r = requests.post(server_address + '/v1/user/groups', json=group_data, headers={"Authorization": get_from_db(username)['auth_token']})
	return r

def left_group(username, group_id):
	r = requests.delete(server_address + '/v1/user/groups/{}'.format(group_id), headers={"Authorization": get_from_db(username)['auth_token'], 'group_id': group_id})
	return r

def my_bonuses(username):
	r = requests.get(server_address + '/v1/user/bonuses', headers={"Authorization": get_from_db(username)['auth_token']})
	return r

def remove_bonus(username, bonus_id, amount, uncountable):
	if not uncountable:
		bonus_amount = {
			'amount': amount
		}
		r = requests.delete(server_address + '/v1/user/bonuses/{}'.format(bonus_id), headers={"Authorization": get_from_db(username)['auth_token']}, json=bonus_amount)
	else:
		r = requests.delete(server_address + '/v1/user/bonuses/{}'.format(bonus_id), headers={"Authorization": get_from_db(username)['auth_token']})
	return r

@bot.message_handler(commands=['start'])
def start_message_command(message):
	username = message.chat.username
	if is_in_db(username):
		bot.send_message(message.chat.id, 'Привет. Вы уже зарегистрированы в нашей системе и авторизованы')	
	else:
		bot.send_message(message.chat.id, 'Привет, ты написал мне /start. Регистрирую тебя в нашей системе...')
		register(username)
		resp = login(username)
		if resp.status_code == 200:
			bot.send_message(message.chat.id, 'Ты успешно зарегистрирован в нашей системе')
			bot.send_message(message.chat.id, 'Я умею обрабатывать следующие комманды: /start, /groups_to_join, /my_groups, /join_group group_id, /left_group group_id, /my_bonuses, /remove_bonus bonus_id (amount)')
			save_to_db(username, resp.json()['result']['auth_token'], resp.json()['result']['user_id'])
		else:
			bot.send_message(message.chat.id, 'Не удалось тебя зарегистрировать в нашей системе. Попробуйте позже')	
			print(resp)


@bot.message_handler(commands=['my_groups'])
def my_groups_command(message):
	username = message.chat.username
	
	if is_in_db(username):
		resp = get_groups(username)

		if resp.status_code == 200:
			groups = resp.json()['result']['content']
			groups_in_str = ''
			for i, group in enumerate(groups):
				title = group['title'] 
				description = group['description'] 
				group_id = str(group['id'])
				groups_in_str += str(i + 1) + ': *Имя группы*: ' + title + '\n' + '    *Id*: ' + group_id + '\n' + '    *Описание*: ' + description + '\n\n'
			if len(groups_in_str) == 0:
				bot.send_message(message.chat.id, 'Вы не состоите ни в одной группе')
			else:
				bot.send_message(message.chat.id, '*Список ваших групп*: \n\n' + groups_in_str, parse_mode='Markdown')
		else:
			bot.send_message(message.chat.id, 'Не удалось найти ваши группы')
	else:
		bot.send_message(message.chat.id, 'Не удалось тебя найти в нашей системе. Набери команду /start')	


@bot.message_handler(commands=['groups_to_join'])
def groups_to_join_command(message):
	username = message.chat.username
	
	if is_in_db(username):
		resp = get_groups_to_join(username)
		if resp.status_code == 200:
			groups = resp.json()['result']['content']
			groups_in_str = ''
			for i, group in enumerate(groups):
				title = group['title'] 
				description = group['description']
				group_id = str(group['id'])
				groups_in_str += str(i + 1) + ': *Имя группы*: ' + title + '\n' + '    *Id*: ' + group_id + '\n' + '    *Описание*: ' + description + '\n\n'
			if len(groups_in_str) == 0:
				bot.send_message(message.chat.id, 'Пока нет ни одной группы')
			else:
				bot.send_message(message.chat.id, '*Список групп*: \n\n' + groups_in_str, parse_mode='Markdown')
		else:
			bot.send_message(message.chat.id, 'Не удалось найти список групп на вступление')
	else:
		bot.send_message(message.chat.id, 'Не удалось тебя найти в нашей системе. Набери команду /start')	


@bot.message_handler(commands=['join_group'])
def join_group_command(message):
	username = message.chat.username
	args = message.text.split()

	if len(args) == 1:
		bot.send_message(message.chat.id, 'После команды нужно ввести id группы')
	else:
		group_id = args[1]

		if is_in_db(username):
			resp = join_group(username, group_id)
			if resp.status_code == 200:
				bot.send_message(message.chat.id, 'Заявка на вступление в группу успешно подана')
			else:
				bot.send_message(message.chat.id, 'Не удалось вступить в группу')
		else:
			bot.send_message(message.chat.id, 'Не удалось тебя найти в нашей системе. Набери команду /start')	


@bot.message_handler(commands=['left_group'])
def left_group_command(message):
	username = message.chat.username
	args = message.text.split()

	if len(args) == 1:
		bot.send_message(message.chat.id, 'После команды нужно ввести id группы')
	else:
		group_id = args[1]

		if not group_id.isdigit():
			bot.send_message(message.chat.id, 'Id группы должно быть целым числом')
		elif is_in_db(username):
			resp = left_group(username, group_id)
			if resp.status_code == 200:
				bot.send_message(message.chat.id, 'Вы успешно вышли из группы')
			else:
				bot.send_message(message.chat.id, 'Не удалось выйти из группы')
		else:
			bot.send_message(message.chat.id, 'Не удалось тебя найти в нашей системе. Набери команду /start')	

@bot.message_handler(commands=['my_bonuses'])
def my_bonuses_command(message):
	username = message.chat.username

	if is_in_db(username):
		resp = my_bonuses(username)
		if resp.status_code == 200:
			bonuses = resp.json()['result']['content']
			bonuses_in_str = ''
			for i, b in enumerate(bonuses):

				amount = str(b['amount'])
				bonus = b['bonus']
				group = b['group']
				group_bonus_id = str(b['group_bonus_id'])

				if bonus['isCountable']:
					bonuses_in_str += str(i + 1) + ': *Имя бонуса*: ' + bonus['title'] + '\n' +  '    *Описание*: ' + bonus['description'] + '\n' + '    *Группа*: ' + group['title'] + '\n' + '    *Id бонуса*: ' + group_bonus_id + '\n' + '    *Кол-во бонуса*: ' + amount +  '\n\n'
				else:
					bonuses_in_str += str(i + 1) + ': *Имя бонуса*: ' + bonus['title'] + '\n' +  '    *Описание*: ' + bonus['description'] + '\n' + '    *Группа*: ' + group['title'] + '\n' + '    *Id бонуса*: ' + group_bonus_id + '\n\n'

			if len(bonuses_in_str) == 0:
				bot.send_message(message.chat.id, 'Пока нет ни одного бонуса')
			else:
				bot.send_message(message.chat.id, '*Список бонусов*: \n\n' + bonuses_in_str, parse_mode='Markdown')
		else:
			bot.send_message(message.chat.id, 'Не удалось запросить бонусы')
	else:
		bot.send_message(message.chat.id, 'Не удалось тебя найти в нашей системе. Набери команду /start')	

def generate_qr_code(username, amount, bonus_id):
	encoded_bytes = b64encode((username + amount + bonus_id).encode("utf-8"))
	encoded_string = str(encoded_bytes, "utf-8")
	qr = pyqrcode.create(encoded_string)
	buffer = io.BytesIO()
	qr.png(buffer, scale=6)
	return buffer.getvalue()

@bot.message_handler(commands=['remove_bonus'])
def remove_bonus_command(message):
	username = message.chat.username
	args = message.text.split()

	if len(args) < 2:
		bot.send_message(message.chat.id, 'После команды нужно ввести id бонуса и (кол-во для списания опционально)')
	else:
		bonus_id = args[1]
		uncountable = False
		amount = '1'
		if len(args) > 2:
			amount = args[2]
			uncountable = True

		if not bonus_id.isdigit():
			bot.send_message(message.chat.id, 'Id бонуса должно быть целым числом')
		if not uncountable and not amount.isdigit():
			bot.send_message(message.chat.id, 'Кол-во бонуса должно быть целым числом')	
		elif is_in_db(username):
			resp = remove_bonus(username, bonus_id, amount, uncountable)
			print(resp)
			if resp.status_code == 200:
				bot.send_message(message.chat.id, 'Бонусы списаны успешно. Qr код для списания бонусов: ')
				qr_code = generate_qr_code(username, amount, bonus_id)
				bot.send_photo(message.chat.id, qr_code)
			else:
				bot.send_message(message.chat.id, 'Не удалось запросить списание бонусов')
		else:
			bot.send_message(message.chat.id, 'Не удалось тебя найти в нашей системе. Набери команду /start')	

@bot.message_handler(commands=['list_commands'])
def list_commands(message):
	bot.send_message(message.chat.id, 'Я умею обрабатывать следующие комманды: /start, /groups_to_join, /my_groups, /join_group group_id, /left_group group_id, /my_bonuses, /remove_bonus bonus_id (amount)')

@bot.message_handler(content_types=['text'])
def send_text(message):    
	bot.send_message(message.chat.id, 'Я умею обрабатывать следующие комманды: /start, /groups_to_join, /my_groups, /join_group group_id, /left_group group_id, /my_bonuses, /remove_bonus bonus_id (amount)')

bot.polling()


