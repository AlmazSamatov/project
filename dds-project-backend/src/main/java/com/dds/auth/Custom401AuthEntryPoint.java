package com.dds.auth;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class Custom401AuthEntryPoint implements AuthenticationEntryPoint {
    private List<AntPathRequestMatcher> antMatchers;
    private String headerValue;

    public Custom401AuthEntryPoint(String[] antPatterns, String headerValue) {
        this.antMatchers = Arrays.stream(antPatterns).map(AntPathRequestMatcher::new).collect(Collectors.toList());
        this.headerValue = headerValue;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setHeader("WWW-Authenticate",
                antMatchers.stream().anyMatch(m -> m.matches(request)) ? this.headerValue : null);
        response.sendError(401, authException.getMessage());
    }
}
