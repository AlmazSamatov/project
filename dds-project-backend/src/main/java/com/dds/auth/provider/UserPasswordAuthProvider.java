package com.dds.auth.provider;

import com.dds.auth.AuthenticationImpl;
import com.dds.auth.CurrentUser;
import com.dds.common.exception.NotFoundException;
import com.dds.model.model.User;
import com.dds.service.service.UserService;
import com.dds.service.util.PasswordUtil;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class UserPasswordAuthProvider implements AuthenticationProvider {

    @Value("${dds.project.docs.user}")
    private String swaggerUser;

    @Value("${dds.project.docs.password}")
    private String swaggerPassword;

    private final UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Object passwordObj = authentication.getCredentials();
        if (passwordObj == null) {
            passwordObj = StringUtils.EMPTY;
        }
        String password = passwordObj.toString();
        String login = authentication.getName();

        if (StringUtils.isAnyBlank(password, login)) {
            throw new BadCredentialsException("empty-param");
        }

        try {
            return authSwaggerUser(login, password);
        } catch (BadCredentialsException e) {
        }

        User user;
        try {
            user = userService.getUserByEmailOrTelegramAlias(login);
        } catch (NotFoundException e) {
            throw new BadCredentialsException("user-not-found");
        }

        if (!(Objects.equals(PasswordUtil.getHash(password), user.getPasswordHash()))) {
            throw new BadCredentialsException("wrong-passwordHash");
        }

        List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList();
        authorityList.add(new SimpleGrantedAuthority(user.getRole().name()));
        UserDetails currentUser = CurrentUser
                .cuBuilder()
                .id(user.getId())
                .username(user.getId().toString())
                .enabled(true)
                .accountNonLocked(true)
                .accountNonExpired(true)
                .credentialsNonExpired(true)
                .authorities(authorityList)
                .build();

        return new AuthenticationImpl(currentUser);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return StringUtils.equals(authentication.getName(), UsernamePasswordAuthenticationToken.class.getName());
    }

    private Authentication authSwaggerUser(String user, String password) {
        if (swaggerUser.equals(user) && swaggerPassword.equals(password)) {
            return new AuthenticationImpl(CurrentUser
                    .cuBuilder()
                    .id(-1L)
                    .username(user)
                    .enabled(true)
                    .accountNonLocked(true)
                    .accountNonExpired(true)
                    .credentialsNonExpired(true)
                    .authorities(Lists.newArrayList(new SimpleGrantedAuthority("SWAGGER")))
                    .build());
        }
        throw new BadCredentialsException("user-not-found");
    }
}
