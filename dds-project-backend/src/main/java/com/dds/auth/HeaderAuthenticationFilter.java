package com.dds.auth;

import com.dds.common.exception.NotFoundException;
import com.dds.model.model.User;
import com.dds.service.exception.AuthTokenParseException;
import com.dds.service.service.AuthTokenService;
import com.dds.service.service.UserService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Setter
@Getter
@Slf4j
public class HeaderAuthenticationFilter extends FilterSecurityInterceptor {

    private AuthTokenService authTokenService;
    private UserService userService;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        SecurityContext securityContext = loadSecurityContext(request);
        if (securityContext != null)
            SecurityContextHolder.setContext(securityContext);

        chain.doFilter(request, response);
    }

    private SecurityContext loadSecurityContext(HttpServletRequest request) {
        String xAuth = request.getHeader("Authorization");

        if (StringUtils.isBlank(xAuth)) {
            return null;
        }

        Long userId;
        try {
            userId = authTokenService.getUserId(xAuth);
        } catch (AuthTokenParseException e) {
            log.info("Wrong token: " + e.getMessage());
            return null;
        }

        User user;
        try {
            user = userService.getUser(userId);
        } catch (NotFoundException e) {
            log.info("User with id {} not found", userId);
            return null;
        }

        List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList();
        authorityList.add(new SimpleGrantedAuthority(user.getRole().name()));
        UserDetails userDetails = CurrentUser
                .cuBuilder()
                .id(userId)
                .username(userId != null ? userId.toString() : null)
                .enabled(true)
                .accountNonLocked(true)
                .accountNonExpired(true)
                .credentialsNonExpired(true)
                .authorities(authorityList)
                .build();

        return new CustomSecurityContext(userDetails);
    }
}
