package com.dds.common.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
public class Response<T> {
    private T result;
    private Error error;

    public Response(T result) {
        this.result = result;
    }

    @Setter
    @Getter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Error {

        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String message;

        private String type;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        private List<Detail> details;

        @Setter
        @Getter
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class Detail {
            private String message;
            private String type;
            private String target;

            public static List<Response.Error.Detail> getErrorDetails(BindingResult bindingResult) {
                List<Response.Error.Detail> detailList = new ArrayList<>();

                if (bindingResult.hasErrors()) {
                    val allErrors = bindingResult.getAllErrors();
                    for (val error : allErrors) {
                        val codes = Arrays.asList(error.getCodes());
                        if (codes.contains("NotBlank") || codes.contains("NotNull") || codes.contains("NotEmpty")) {
                            detailList.add(Response.Error.Detail.builder()
                                    .type(Response.Error.Detail.Code.EMPTY_PARAM.getType())
                                    .message(error.getDefaultMessage())
                                    .target((error instanceof FieldError ? ((FieldError) error).getField() : null))
                                    .build());
                        } else {
                            detailList.add(Response.Error.Detail.builder()
                                    .type(Response.Error.Detail.Code.NOT_CORRECT.getType())
                                    .message(error.getDefaultMessage())
                                    .target((error instanceof FieldError ? ((FieldError) error).getField() : null))
                                    .build());
                        }
                    }
                }

                return detailList;
            }

            @RequiredArgsConstructor
            @AllArgsConstructor
            public enum Code {
                EMPTY_PARAM("empty-param"),
                NOT_CORRECT("not-correct");
                @Getter
                private String type;
            }
        }
    }
}