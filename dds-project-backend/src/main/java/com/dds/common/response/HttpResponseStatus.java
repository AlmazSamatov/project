package com.dds.common.response;

public final class HttpResponseStatus {
    // Main errors types
    public static final String NOT_FOUND = "not-found";
    public static final String FORBIDDEN = "forbidden";
    public static final String VALIDATION_ERROR = "validation-error";
    public static final String VERIFICATION_ERROR = "verification-error";
    public static final String BAD_CREDENTIALS = "bad-credentials";
    public static final String AUTH_ERROR = "auth-error";
    public static final String NOT_ACCEPTABLE = "not-acceptable";

    // Details type
    public static final String DUPLICATE = "duplicate";
    public static final String NOT_IMAGE_FILE = "not-image-file";
    public static final String NOT_NUMBER = "not-number";
}
