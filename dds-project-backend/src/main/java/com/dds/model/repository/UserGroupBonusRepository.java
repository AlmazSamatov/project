package com.dds.model.repository;

import com.dds.model.model.UserGroupBonus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserGroupBonusRepository extends JpaRepository<UserGroupBonus, Long> {
    Page<UserGroupBonus> getAllByUserId(Long userId, Pageable pageable);

    Optional<UserGroupBonus> getByGroupBonusIdAndUserId(Long groupBonusId, Long userId);
}
