package com.dds.model.repository;

import com.dds.model.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> getById(long id);

    Optional<User> getByEmail(String email);

    Optional<User> getByTelegramAlias(String alias);

    Optional<User> getByEmailOrTelegramAlias(String email, String alias);

    Page<User> getAllByEmailContainingOrTelegramAliasContaining(String email, String telegramAlias, Pageable pageable);
}
