package com.dds.model.repository;

import com.dds.model.model.Bonus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BonusRepository extends JpaRepository<Bonus, Long> {
    Optional<Bonus> getById(Long id);

    Page<Bonus> getAllByTitleContainingIgnoreCase(String title, Pageable pageable);
}
