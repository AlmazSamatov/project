package com.dds.model.repository;

import com.dds.model.model.GroupBonus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GroupBonusRepository extends JpaRepository<GroupBonus, Long> {
    Optional<GroupBonus> getById(Long id);

    Page<GroupBonus> getAllByGroupId(Long groupId, Pageable pageable);

    Optional<GroupBonus> getByGroupIdAndBonusId(Long groupId, Long bonusId);
}
