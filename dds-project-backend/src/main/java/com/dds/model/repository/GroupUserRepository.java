package com.dds.model.repository;

import com.dds.model.model.GroupUser;
import com.dds.model.model.enums.GroupUserRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface GroupUserRepository extends JpaRepository<GroupUser, Long> {
    Optional<GroupUser> getByGroupIdAndUserId(Long groupId, Long userId);

    Page<GroupUser> getAllByGroupId(Long id, Pageable pageable);

    Page<GroupUser> getAllByUserIdAndRoleIn(Long id, List<GroupUserRole> roles, Pageable pageable);
}
