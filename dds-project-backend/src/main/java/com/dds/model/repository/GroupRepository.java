package com.dds.model.repository;

import com.dds.model.model.Group;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GroupRepository extends JpaRepository<Group, Long> {
    Optional<Group> getById(Long id);

    Optional<Group> getByInvitationCode(String code);

    Page<Group> getAllByTitleContainingIgnoreCase(String title, Pageable pageable);

    Page<Group> getAllByTitleContainingIgnoreCaseAndIsVisible(String title, Boolean visible, Pageable pageable);
}
