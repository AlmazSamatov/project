package com.dds.model.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@EnableJpaRepositories(
        entityManagerFactoryRef = "primaryEntityManagerFactory",
        transactionManagerRef = "primaryTransactionManager",
        basePackages = "com.dds.model.repository"
)
@Configuration
@EnableTransactionManagement
public class PrimaryDataSourceConfig {

    @Bean(name = "main")
    @ConfigurationProperties("spring.datasource")
    public HikariConfig hikariConfig() {
        return new HikariConfig();
    }


    @Bean(name="platformJpaProperties")
    @Primary
    @ConfigurationProperties("spring.jpa")
    public JpaProperties jpaProperties() {
        return new JpaProperties();
    }

    @Bean(name = "primaryDataSource")
    @Primary
    public DataSource dataSource(@Qualifier("main") HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }

    @Bean(name = "primaryEntityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            EntityManagerFactoryBuilder builder,
            JpaProperties properties,
            DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("com.dds.model")
                .persistenceUnit("primary")
                .properties(properties.getHibernateProperties(dataSource))
                .build();
    }

    @Bean(name = "primaryTransactionManager")
    @Primary
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory,
                                                         DataSource dataSource) {
        PlatformTransactionManager platformTransactionManager = new JpaTransactionManager(entityManagerFactory);
        ((JpaTransactionManager) platformTransactionManager).setDataSource(dataSource);

        return platformTransactionManager;
    }
}
