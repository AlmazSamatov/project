package com.dds.model.model;

import com.dds.model.model.abstracts.AbstractAuditableEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Table(name = "group_bonuses",
        uniqueConstraints = {
                @UniqueConstraint(
                        columnNames = {GroupBonus.GROUP_ID, GroupBonus.BONUS_ID}
                )
        }
)
@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupBonus extends AbstractAuditableEntity {
    public static final String GROUP_ID = "group_id";
    public static final String BONUS_ID = "bonus_id";
    private static final String REMAINING_AMOUNT = "remaining_amount";

    @ManyToOne
    @JoinColumn(name = GROUP_ID)
    private Group group;

    @ManyToOne
    @JoinColumn(name = BONUS_ID)
    private Bonus bonus;

    @Column(name = REMAINING_AMOUNT)
    private Integer remainingAmount;

    @OneToMany(mappedBy = "groupBonus",
            cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserGroupBonus> users;
}
