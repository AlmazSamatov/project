package com.dds.model.model;

import com.dds.model.model.abstracts.AbstractAuditableEntity;
import lombok.*;

import javax.persistence.*;

@Table(name = "group_bonus_users",
        uniqueConstraints = {
                @UniqueConstraint(
                        columnNames = {UserGroupBonus.GROUP_BONUS_ID, UserGroupBonus.USER_ID}
                )
        })
@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserGroupBonus extends AbstractAuditableEntity {
    public static final String GROUP_BONUS_ID = "group_bonus_id";
    public static final String USER_ID = "user_id";
    private static final String AMOUNT = "amount";

    @ManyToOne
    @JoinColumn(name = GROUP_BONUS_ID)
    private GroupBonus groupBonus;

    @ManyToOne
    @JoinColumn(name = USER_ID)
    private User user;

    @Column(name = AMOUNT)
    private Integer amount;
}
