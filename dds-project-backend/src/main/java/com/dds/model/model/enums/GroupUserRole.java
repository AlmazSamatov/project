package com.dds.model.model.enums;

public enum GroupUserRole {
    PENDING, USER, MODERATOR
}
