package com.dds.model.model.enums;

public enum UserRole {
    USER, ADMIN
}