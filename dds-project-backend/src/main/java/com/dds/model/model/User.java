package com.dds.model.model;

import com.dds.model.model.abstracts.AbstractAuditableEntity;
import com.dds.model.model.enums.UserRole;
import com.dds.model.util.Sort;
import lombok.*;

import javax.persistence.*;
import java.util.List;


@Table(name = "users", indexes = {
        @Index(name = "email_index", columnList = User.EMAIL),
        @Index(name = "alias_index", columnList = User.TELEGRAM_ALIAS)
})
@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbstractAuditableEntity {
    public static final String EMAIL = "email";
    public static final String PASSWORD_HASH = "password_hash";
    public static final String TELEGRAM_ALIAS = "telegram_alias";
    public static final String ROLE = "role";
    public static final String NAME = "name";

    @Column(name = EMAIL, length = 100, unique = true)
    private String email;

    @Column(name = TELEGRAM_ALIAS, length = 100, unique = true)
    private String telegramAlias;

    @Column(name = PASSWORD_HASH, length = 512)
    private String passwordHash;

    @Column(name = NAME, length = 250)
    private String name;

    @Column(name = ROLE)
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @OneToMany(mappedBy = "user",
            cascade = CascadeType.ALL, orphanRemoval = true)
    private List<GroupUser> groups;

    @Getter
    @AllArgsConstructor
    public enum UsersSort implements Sort {
        CREATION_DATE("createdDate"),
        EMAIL("email"),
        NAME("name");

        private String field;
    }
}



