package com.dds.model.model;

import com.dds.model.model.abstracts.AbstractAuditableEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;


@Table(name = "groups", indexes = {
        @Index(name = "invitation_code_index", columnList = Group.INVITATION_CODE),
})
@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Group extends AbstractAuditableEntity {
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String IS_VISIBLE = "is_visible";
    public static final String INVITATION_CODE = "invitation_code";

    @Column(name = TITLE)
    private String title;

    @Column(name = DESCRIPTION)
    private String description;

    @Column(name = IS_VISIBLE)
    private Boolean isVisible;

    @Column(name = INVITATION_CODE, unique = true)
    private String invitationCode;

    @OneToMany(mappedBy = "group",
            cascade = CascadeType.ALL, orphanRemoval = true)
    private List<GroupUser> users;

    @OneToMany(mappedBy = "group",
            cascade = CascadeType.ALL, orphanRemoval = true)
    private List<GroupBonus> bonuses;
}
