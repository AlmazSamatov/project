package com.dds.model.model;

import com.dds.model.model.abstracts.AbstractAuditableEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Table(name = "bonuses")
@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Bonus extends AbstractAuditableEntity {
    private static final String TITLE = "title";
    private static final String DESCRIPTION = "description";
    private static final String IS_COUNTABLE = "is_countable";

    @Column(name = TITLE)
    private String title;

    @Column(name = DESCRIPTION)
    private String description;

    @Column(name = IS_COUNTABLE)
    private Boolean isCountable;

    @OneToMany(mappedBy = "bonus", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<GroupBonus> groupBonuses;
}