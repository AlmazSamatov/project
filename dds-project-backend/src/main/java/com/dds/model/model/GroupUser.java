package com.dds.model.model;

import com.dds.model.model.abstracts.AbstractEntity;
import com.dds.model.model.enums.GroupUserRole;
import lombok.*;

import javax.persistence.*;

@Table(name = "group_users",
        uniqueConstraints = {
                @UniqueConstraint(
                        columnNames = {GroupUser.GROUP_ID, GroupUser.USER_ID}
                )
        }
)
@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupUser extends AbstractEntity {
    public static final String GROUP_ID = "group_id";
    public static final String USER_ID = "user_id";
    public static final String ROLE = "role";

    @ManyToOne
    @JoinColumn(name = GROUP_ID)
    private Group group;

    @ManyToOne
    @JoinColumn(name = USER_ID)
    private User user;

    @Column(name = ROLE)
    @Enumerated(EnumType.STRING)
    private GroupUserRole role;
}
