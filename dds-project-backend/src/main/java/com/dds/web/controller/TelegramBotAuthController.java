package com.dds.web.controller;

import com.dds.common.exception.NotFoundException;
import com.dds.common.response.HttpResponseStatus;
import com.dds.common.response.Response;
import com.dds.model.model.User;
import com.dds.service.dto.form.post.RegisterTelegramUserForm;
import com.dds.service.dto.form.post.TelegramSignInForm;
import com.dds.service.dto.json.TokenJson;
import com.dds.service.exception.DuplicateException;
import com.dds.service.service.AuthTokenService;
import com.dds.service.service.UserService;
import com.dds.web.controller.util.CommonExceptionResponses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;

@Api(tags = {"Users.Auth"})
@RestController
@RequestMapping(TelegramBotAuthController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class TelegramBotAuthController {
    public static final String ROOT_URL = "/v1/telegram-bot";
    public static final String LOGIN_URL = "/login";
    public static final String REGISTRATION = "/registration";

    @Value("${dds.project.telegram-bot-token}")
    private String telegramBotToken;

    private final AuthTokenService authTokenService;
    private final UserService userService;

    private void checkTelegramBotCredentials(String token) {
        if (!telegramBotToken.equals(token)) {
            throw new BadCredentialsException("Wrong telegram-bot-token");
        }
    }

    @ApiOperation(value = "Sign in by telegram alias for telegram bot")
    @ApiResponses({@ApiResponse(code = org.apache.http.HttpStatus.SC_BAD_REQUEST, message = "Validation issues"),
            @ApiResponse(code = org.apache.http.HttpStatus.SC_UNAUTHORIZED, message = "Authentication failed")})
    @PostMapping(LOGIN_URL)
    public ResponseEntity<Response<TokenJson>> signIn(
            @Valid @RequestBody TelegramSignInForm form,
            BindingResult errors) {
        if (errors.hasErrors()) {
            return CommonExceptionResponses.validationError(errors);
        }

        String telegramAlias = StringUtils.trim(form.getTelegramAlias().toLowerCase());

        try {
            checkTelegramBotCredentials(form.getTelegramBotToken());

            User user = userService.getUserByEmailOrTelegramAlias(telegramAlias);

            TokenJson tokenJson = new TokenJson();
            tokenJson.setUserId(user.getId());
            tokenJson.setToken(authTokenService.generateToken(user.getId()));

            return new ResponseEntity<>(new Response<>(tokenJson), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        } catch (BadCredentialsException e) {
            return CommonExceptionResponses.badCredentialsError(e);
        }
    }


    @ApiOperation(value = "Register telegram user")
    @ApiResponses({@ApiResponse(code = org.apache.http.HttpStatus.SC_BAD_REQUEST, message = "Validation or verification issues")})
    @PostMapping(REGISTRATION)
    public ResponseEntity<Response<String>> register(
            @Valid @RequestBody RegisterTelegramUserForm form,
            BindingResult errors) {

        if (errors.hasErrors()) {
            return CommonExceptionResponses.validationError(errors);
        }
        try {
            checkTelegramBotCredentials(form.getTelegramBotToken());

            userService.registerTelegramUser(form);
            return new ResponseEntity<>(new Response<>("ok"), HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return CommonExceptionResponses.badCredentialsError(e);
        } catch (DuplicateException e) {
            return new ResponseEntity<>(new Response<>(null, Response.Error
                    .builder()
                    .type(HttpResponseStatus.VALIDATION_ERROR)
                    .details(Collections.singletonList(
                            Response.Error.Detail
                                    .builder()
                                    .type(HttpResponseStatus.DUPLICATE)
                                    .message(e.getMessage())
                                    .target(RegisterTelegramUserForm.TELEGRAM_ALIAS)
                                    .build()
                    ))
                    .build()
            ), HttpStatus.BAD_REQUEST);
        }
    }
}
