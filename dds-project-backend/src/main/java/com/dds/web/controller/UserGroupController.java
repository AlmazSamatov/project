package com.dds.web.controller;

import com.dds.auth.CurrentUser;
import com.dds.common.exception.NotFoundException;
import com.dds.common.response.HttpResponseStatus;
import com.dds.common.response.Response;
import com.dds.model.model.Group;
import com.dds.model.model.GroupUser;
import com.dds.service.dto.form.get.FilterForm;
import com.dds.service.dto.form.post.GroupEntryApplicationForm;
import com.dds.service.dto.json.GroupJson;
import com.dds.service.dto.json.PageJson;
import com.dds.service.dto.json.UserGroupJson;
import com.dds.service.exception.DuplicateException;
import com.dds.service.service.GroupService;
import com.dds.web.controller.util.CommonExceptionResponses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = {"User.Group"})
@RestController
@RequestMapping(UserGroupController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class UserGroupController {
    public static final String ROOT_URL = "/v1/user/groups";
    public static final String GROUP = "/{group_id}";
    public static final String EXTENDED = "/extended";

    private final GroupService groupService;

    @ApiOperation("Get groups of user with roles")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(EXTENDED)
    public ResponseEntity<Response<PageJson<UserGroupJson>>> getUserGroupsExtended(
            @Valid @ModelAttribute FilterForm form,
            BindingResult errors,
            @AuthenticationPrincipal CurrentUser currentUser) {
        if (errors.hasErrors())
            CommonExceptionResponses.validationError(errors);

        Page<GroupUser> groups = groupService.getGroupsOfUser(currentUser.getId(), form);
        PageJson<UserGroupJson> pageJson = new PageJson<>(groups,
                groups.map(UserGroupJson::mapFromGroupUser).getContent());
        return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);
    }

    @ApiOperation("Get groups of user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<Response<PageJson<GroupJson>>> getUserGroups(@Valid @ModelAttribute FilterForm form,
                                                                       BindingResult errors,
                                                                       @AuthenticationPrincipal CurrentUser currentUser) {
        if (errors.hasErrors())
            CommonExceptionResponses.validationError(errors);

        Page<Group> groups = groupService.getGroupsOfUser(currentUser.getId(), form).map(GroupUser::getGroup);
        PageJson<GroupJson> pageJson = new PageJson<>(groups,
                groups.map(x -> GroupJson.mapFromGroup(x, false)).getContent());
        return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);
    }


    @ApiOperation("Leave group")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @DeleteMapping(GROUP)
    public ResponseEntity<Response<String>> leaveGroup(@PathVariable("group_id") Long groupId,
                                                       @AuthenticationPrincipal CurrentUser currentUser) {

        try {
            groupService.removeUser(groupId, currentUser.getId());
            return new ResponseEntity<>(new Response<>("Ok"), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Application for entry")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @PostMapping
    public ResponseEntity<Response<String>> entryApplication(@Valid @RequestBody GroupEntryApplicationForm form,
                                                             BindingResult errors,
                                                             @AuthenticationPrincipal CurrentUser currentUser) {
        if (errors.hasErrors())
            CommonExceptionResponses.validationError(errors);

        try {
            groupService.addPendingUser(form.getId(), currentUser.getId());
            return new ResponseEntity<>(new Response<>("Ok"), HttpStatus.OK);
        } catch (DuplicateException e) {
            return new ResponseEntity<>(new Response<>(null, Response.Error
                    .builder()
                    .type(HttpResponseStatus.DUPLICATE)
                    .message(e.getMessage())
                    .build()
            ), HttpStatus.NOT_FOUND);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }
}
