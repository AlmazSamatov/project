package com.dds.web.controller;

import com.dds.common.exception.NotFoundException;
import com.dds.common.response.HttpResponseStatus;
import com.dds.common.response.Response;
import com.dds.model.model.Group;
import com.dds.service.dto.form.get.FilterForm;
import com.dds.service.dto.json.GroupJson;
import com.dds.service.dto.json.PageJson;
import com.dds.service.service.GroupService;
import com.dds.web.controller.admin.AdminGroupController;
import com.dds.web.controller.util.CommonExceptionResponses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.dds.common.response.Response.Error.Detail.getErrorDetails;

@Api(tags = {"Group"})
@RestController
@RequestMapping(GroupController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class GroupController {
    public static final String ROOT_URL = "/v1/groups";
    public static final String GROUP = "/{group_id}";

    private final GroupService groupService;

    @ApiOperation("Get group")
    @GetMapping(GROUP)
    public ResponseEntity<Response<GroupJson>> getGroup(@PathVariable("group_id") Long groupId) {
        try {
            Group group = groupService.getVisibleGroup(groupId);
            return new ResponseEntity<>(new Response<>(GroupJson.mapFromGroup(group, false)), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Get groups")
    @GetMapping
    public ResponseEntity<Response<PageJson<GroupJson>>> getGroups(@Valid @ModelAttribute FilterForm form,
                                                                   BindingResult errors) {
        if (errors.hasErrors()) {
            return CommonExceptionResponses.validationError(errors);
        }

        Page<Group> groups = groupService.getVisibleGroups(form);
        PageJson<GroupJson> pageJson = new PageJson<>(groups,
                groups.map(x -> GroupJson.mapFromGroup(x, false)).getContent());
        return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);
    }
}
