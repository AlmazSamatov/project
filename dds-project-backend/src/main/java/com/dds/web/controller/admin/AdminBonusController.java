package com.dds.web.controller.admin;

import com.dds.common.exception.NotFoundException;
import com.dds.common.response.Response;
import com.dds.model.model.Bonus;
import com.dds.service.dto.form.get.FilterForm;
import com.dds.service.dto.form.post.CreateBonusForm;
import com.dds.service.dto.json.BonusJson;
import com.dds.service.dto.json.PageJson;
import com.dds.service.service.BonusService;
import com.dds.web.controller.util.CommonExceptionResponses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = {"Admin.Bonuses"})
@RestController
@RequestMapping(AdminBonusController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class AdminBonusController {
    public static final String ROOT_URL = "/v1/admin/bonuses";
    public static final String BONUS = "/{bonus_id}";

    private final BonusService bonusService;

    @ApiOperation("Get bonuses")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<Response<PageJson<BonusJson>>> getBonuses(@Valid @ModelAttribute FilterForm form,
                                                                    BindingResult errors) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        Page<Bonus> bonuses = bonusService.getBonuses(form);
        PageJson<BonusJson> pageJson = new PageJson<>(
                bonuses,
                bonuses.map(BonusJson::mapFromBonus).getContent()
        );

        return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);
    }

    @ApiOperation("Create bonus")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @PostMapping
    public ResponseEntity<Response<BonusJson>> createBonus(@Valid @RequestBody CreateBonusForm form,
                                                           BindingResult errors) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        Bonus bonus = bonusService.createBonus(form);
        return new ResponseEntity<>(new Response<>(BonusJson.mapFromBonus(bonus)), HttpStatus.OK);
    }

    @ApiOperation("Get bonus")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(BONUS)
    public ResponseEntity<Response<BonusJson>> getBonus(@PathVariable("bonus_id") Long bonusId) {
        try {
            Bonus bonus = bonusService.getBonus(bonusId);
            return new ResponseEntity<>(new Response<>(BonusJson.mapFromBonus(bonus)), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Delete bonus")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @DeleteMapping(BONUS)
    public ResponseEntity<Response<String>> deleteBonus(@PathVariable("bonus_id") Long bonusId) {
        try {
            bonusService.deleteBonus(bonusId);
            return new ResponseEntity<>(new Response<>("Ok"), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }
}
