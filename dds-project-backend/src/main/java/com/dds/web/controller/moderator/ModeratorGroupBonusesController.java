package com.dds.web.controller.moderator;

import com.dds.auth.CurrentUser;
import com.dds.common.exception.NotFoundException;
import com.dds.common.response.Response;
import com.dds.service.dto.form.post.AddGroupBonusForm;
import com.dds.service.dto.json.GroupBonusJson;
import com.dds.service.exception.ForbiddenException;
import com.dds.service.service.BonusService;
import com.dds.service.service.GroupService;
import com.dds.web.controller.admin.AdminGroupBonusesController;
import com.dds.web.controller.util.CommonExceptionResponses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = {"Moderator.Group.Bonuses"})
@RestController
@RequestMapping(ModeratorGroupBonusesController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class ModeratorGroupBonusesController {
    public static final String ROOT_URL = "/v1/moderator/group/bonuses";
    public static final String BONUS = "/{group_bonus_id}";
    public static final String USERS = BONUS + "/users";
    public static final String USER = USERS + "/{user_id}";

    private final GroupService groupService;
    private final BonusService bonusService;
    private final AdminGroupBonusesController adminGroupBonusesController;

    @ApiOperation("Get group bonuses of user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(USER)
    public ResponseEntity<Response<GroupBonusJson>> getGroupBonusOfUser(
            @PathVariable("group_bonus_id") Long groupBonusId,
            @PathVariable("user_id") Long userId,
            @AuthenticationPrincipal CurrentUser currentUser) {
        try {
            groupService.checkOnModerator(bonusService.getGroupBonus(groupBonusId).getGroup().getId(),
                    currentUser.getId());
            return adminGroupBonusesController.getGroupBonusOfUser(groupBonusId, userId);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        } catch (ForbiddenException e) {
            return CommonExceptionResponses.forbiddenError(e);
        }
    }

    @ApiOperation("Add group bonuses to user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @PutMapping(USER)
    public ResponseEntity<Response<GroupBonusJson>> addGroupBonusToUser(
            @PathVariable("group_bonus_id") Long groupBonusId,
            @PathVariable("user_id") Long userId,
            @Valid @RequestBody AddGroupBonusForm form,
            BindingResult errors,
            @AuthenticationPrincipal CurrentUser currentUser) {
        try {
            groupService.checkOnModerator(bonusService.getGroupBonus(groupBonusId).getGroup().getId(),
                    currentUser.getId());
            return adminGroupBonusesController.addGroupBonusToUser(groupBonusId, userId, form, errors);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        } catch (ForbiddenException e) {
            return CommonExceptionResponses.forbiddenError(e);
        }
    }

    @ApiOperation("Remove group bonuses from user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @DeleteMapping(USER)
    public ResponseEntity<Response<GroupBonusJson>> removeGroupBonusesFromUser(
            @PathVariable("group_bonus_id") Long groupBonusId,
            @PathVariable("user_id") Long userId,
            @Valid @RequestBody AddGroupBonusForm form,
            BindingResult errors,
            @AuthenticationPrincipal CurrentUser currentUser) {
        try {
            groupService.checkOnModerator(bonusService.getGroupBonus(groupBonusId).getGroup().getId(),
                    currentUser.getId());
            return adminGroupBonusesController.removeGroupBonusesFromUser(groupBonusId, userId, form, errors);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        } catch (ForbiddenException e) {
            return CommonExceptionResponses.forbiddenError(e);
        }
    }
}
