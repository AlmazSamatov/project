package com.dds.web.controller;

import com.dds.auth.CurrentUser;
import com.dds.common.exception.NotFoundException;
import com.dds.common.response.Response;
import com.dds.model.model.UserGroupBonus;
import com.dds.service.dto.form.get.FilterForm;
import com.dds.service.dto.form.post.AddGroupBonusForm;
import com.dds.service.dto.json.GroupBonusJson;
import com.dds.service.dto.json.PageJson;
import com.dds.service.service.BonusService;
import com.dds.web.controller.util.CommonExceptionResponses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = {"User.Bonuses"})
@RestController
@RequestMapping(UserBonusesController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class UserBonusesController {
    public static final String ROOT_URL = "/v1/user/bonuses";
    public static final String BONUS = "/{group_bonus_id}";

    private final BonusService bonusService;

    @ApiOperation("Get all group bonuses")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<Response<PageJson<GroupBonusJson>>> getBonuses(
            @Valid @ModelAttribute FilterForm form,
            BindingResult errors,
            @AuthenticationPrincipal CurrentUser currentUser) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        Page<UserGroupBonus> userGroupBonuses = bonusService.getBonusesOfUser(currentUser.getId(), form);
        PageJson<GroupBonusJson> pageJson = new PageJson<>(
                userGroupBonuses,
                userGroupBonuses.map(GroupBonusJson::mapFromUserGroupBonus).getContent()
        );
        return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);
    }

    @ApiOperation("Remove bonus from user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @DeleteMapping(BONUS)
    public ResponseEntity<Response<GroupBonusJson>> removeGroupBonusesFromUser(
            @PathVariable("group_bonus_id") Long groupBonusId,
            @RequestBody AddGroupBonusForm form,
            @AuthenticationPrincipal CurrentUser currentUser) {
        if (form.getAmount() == null || form.getAmount() < 0) {
            form.setAmount(Integer.MAX_VALUE);
        }

        try {
            UserGroupBonus userGroupBonus = bonusService.removeGroupBonusFromUser(
                    groupBonusId, currentUser.getId(), form.getAmount());
            return new ResponseEntity<>(new Response<>(
                    GroupBonusJson.mapFromUserGroupBonus(userGroupBonus)), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }
}
