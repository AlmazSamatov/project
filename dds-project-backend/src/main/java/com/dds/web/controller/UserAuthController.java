package com.dds.web.controller;

import com.dds.auth.CurrentUser;
import com.dds.common.response.HttpResponseStatus;
import com.dds.common.response.Response;
import com.dds.service.dto.form.post.SignInForm;
import com.dds.service.dto.form.post.UserRegistrationForm;
import com.dds.service.dto.json.TokenJson;
import com.dds.service.exception.DuplicateException;
import com.dds.service.service.AuthTokenService;
import com.dds.service.service.UserService;
import com.dds.web.controller.util.CommonExceptionResponses;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.Collections;
import java.util.Objects;

import static com.dds.common.response.Response.Error.Detail.getErrorDetails;

@Api(tags = {"Users.Auth"})
@RestController
@RequestMapping(UserAuthController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class UserAuthController {
    public static final String ROOT_URL = "/v1/users";
    public static final String LOGIN_URL = "/login";
    public static final String REGISTRATION = "/registration";

    private final AuthTokenService authTokenService;
    private final UserService userService;
    private final AuthenticationManager authenticationManager;


    @ApiOperation(value = "Sign in by email and password")
    @ApiResponses({@ApiResponse(code = org.apache.http.HttpStatus.SC_BAD_REQUEST, message = "Validation issues"),
            @ApiResponse(code = org.apache.http.HttpStatus.SC_UNAUTHORIZED, message = "Authentication failed")})
    @PostMapping(LOGIN_URL)
    public ResponseEntity<Response<TokenJson>> signIn(@Valid @RequestBody SignInForm form,
                                                      BindingResult errors) {
        if (errors.hasErrors()) {
            return CommonExceptionResponses.validationError(errors);
        }

        String email = StringUtils.trim(form.getEmail()).toLowerCase();
        String password = form.getPassword();

        try {
            TokenJson tokenJson = authenticate(email, password);
            return new ResponseEntity<>(new Response<>(tokenJson), HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return CommonExceptionResponses.badCredentialsError(e);
        } catch (AuthenticationException e) {
            return new ResponseEntity<>(new Response<>(null, Response.Error
                    .builder()
                    .type(HttpResponseStatus.AUTH_ERROR)
                    .message(e.getMessage())
                    .build()
            ), HttpStatus.UNAUTHORIZED);
        }
    }

    @ApiOperation(value = "Register user")
    @ApiResponses({@ApiResponse(code = org.apache.http.HttpStatus.SC_BAD_REQUEST, message = "Validation or verification issues")})
    @PostMapping(REGISTRATION)
    public ResponseEntity<Response<String>> register(
            @Valid @RequestBody UserRegistrationForm form,
            BindingResult errors) {

        if (!Objects.equals(form.getPassword(), form.getConfirmPassword())) {
            errors.addError(new FieldError("userRegistrationForm",
                    UserRegistrationForm.CONFIRM_PASSWORD, null, false,
                    new String[]{"Match"}, null, "Password confirmation does not match password"));
        }

        if (errors.hasErrors()) {
            return CommonExceptionResponses.validationError(errors);
        }
        try {
            userService.registerUser(form);
            return new ResponseEntity<>(new Response<>("ok"), HttpStatus.OK);
        } catch (DuplicateException e) {
            return new ResponseEntity<>(new Response<>(null, Response.Error
                    .builder()
                    .type(HttpResponseStatus.VALIDATION_ERROR)
                    .details(Collections.singletonList(
                            Response.Error.Detail
                                    .builder()
                                    .type(HttpResponseStatus.DUPLICATE)
                                    .message(e.getMessage())
                                    .target(UserRegistrationForm.EMAIL)
                                    .build()
                    ))
                    .build()
            ), HttpStatus.BAD_REQUEST);
        }
    }

    private TokenJson authenticate(String email, String password) {
        TokenJson tokenJson = new TokenJson();
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(email, password);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);

        if (authentication == null || !authentication.isAuthenticated()) {
            log.info("Failed to authenticate user [{}]", email);
            throw new BadCredentialsException(String.format("Failed to authenticate user %s", email));
        }
        Long userId = null;
        Object userDetailsObject = authentication.getDetails();
        if (userDetailsObject instanceof CurrentUser) {
            userId = ((CurrentUser) userDetailsObject).getId();
        }
        log.info("Successfully signed in user [{}] with account [{}]", userId, email);
        tokenJson.setUserId(userId);
        tokenJson.setToken(authTokenService.generateToken(userId));
        return tokenJson;
    }
}
