package com.dds.web.controller.moderator;

import com.dds.auth.CurrentUser;
import com.dds.common.exception.NotFoundException;
import com.dds.common.response.Response;
import com.dds.model.model.Group;
import com.dds.service.dto.form.get.FilterForm;
import com.dds.service.dto.json.GroupBonusJson;
import com.dds.service.dto.json.GroupJson;
import com.dds.service.dto.json.GroupUserJson;
import com.dds.service.dto.json.PageJson;
import com.dds.service.exception.ForbiddenException;
import com.dds.service.service.GroupService;
import com.dds.web.controller.admin.AdminGroupController;
import com.dds.web.controller.util.CommonExceptionResponses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = {"Moderator.Group"})
@RestController
@RequestMapping(ModeratorGroupController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class ModeratorGroupController {
    public static final String ROOT_URL = "/v1/moderator/groups";
    public static final String GROUP = "/{group_id}";
    public static final String USERS = GROUP + "/users";
    public static final String USER = USERS + "/{user_id}";
    public static final String BONUSES = GROUP + "/bonuses";

    private final GroupService groupService;
    private final AdminGroupController adminGroupController;

    @ApiOperation("Get moderator groups")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<Response<PageJson<GroupJson>>> getModeratorGroups(@Valid @ModelAttribute FilterForm form,
                                                                            BindingResult errors,
                                                                            @AuthenticationPrincipal CurrentUser currentUser) {
        if (errors.hasErrors())
            CommonExceptionResponses.validationError(errors);

        try {
            Page<Group> groups = groupService.getGroupsOfModerator(currentUser.getId(), form);
            PageJson<GroupJson> pageJson = new PageJson<>(groups,
                    groups.map(x -> GroupJson.mapFromGroup(x, true)).getContent());
            return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Get group users")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(USERS)
    public ResponseEntity<Response<PageJson<GroupUserJson>>> getGroupUsers(@PathVariable("group_id") Long groupId,
                                                                           @Valid @ModelAttribute FilterForm form,
                                                                           BindingResult errors,
                                                                           @AuthenticationPrincipal CurrentUser currentUser) {
        try {
            groupService.checkOnModerator(groupId, currentUser.getId());
            return adminGroupController.getGroupUsers(groupId, form, errors);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        } catch (ForbiddenException e) {
            return CommonExceptionResponses.forbiddenError(e);
        }
    }

    @ApiOperation("Accept user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @PostMapping(USER)
    public ResponseEntity<Response<String>> acceptUser(@PathVariable("group_id") Long groupId,
                                                       @PathVariable("user_id") Long userId,
                                                       @AuthenticationPrincipal CurrentUser currentUser) {

        try {
            groupService.checkOnModerator(groupId, currentUser.getId());
            return adminGroupController.acceptUser(groupId, userId);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        } catch (ForbiddenException e) {
            return CommonExceptionResponses.forbiddenError(e);
        }
    }

    @ApiOperation("Remove user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @DeleteMapping(USER)
    public ResponseEntity<Response<String>> removeUser(@PathVariable("group_id") Long groupId,
                                                       @PathVariable("user_id") Long userId,
                                                       @AuthenticationPrincipal CurrentUser currentUser) {

        try {
            groupService.checkOnModerator(groupId, currentUser.getId());
            return adminGroupController.removeUser(groupId, userId);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        } catch (ForbiddenException e) {
            return CommonExceptionResponses.forbiddenError(e);
        }
    }

    @ApiOperation("Get all group bonuses")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(BONUSES)
    public ResponseEntity<Response<PageJson<GroupBonusJson>>> getBonuses(
            @PathVariable("group_id") Long groupId,
            @Valid @ModelAttribute FilterForm form,
            BindingResult errors,
            @AuthenticationPrincipal CurrentUser currentUser) {
        try {
            groupService.checkOnModerator(groupId, currentUser.getId());
            return adminGroupController.getBonuses(groupId, form, errors);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        } catch (ForbiddenException e) {
            return CommonExceptionResponses.forbiddenError(e);
        }
    }
}
