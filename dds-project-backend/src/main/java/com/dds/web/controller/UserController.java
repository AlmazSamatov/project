package com.dds.web.controller;

import com.dds.auth.CurrentUser;
import com.dds.common.response.Response;
import com.dds.model.model.User;
import com.dds.service.dto.json.UserJson;
import com.dds.service.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api(tags = {"User"})
@RestController
@RequestMapping(UserController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class UserController {
    public static final String ROOT_URL = "/v1/user";

    private final UserService userService;

    @ApiOperation("Get user's profile")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<Response<UserJson>> getProfile(@AuthenticationPrincipal CurrentUser currentUser) {
        User user = userService.getUser(currentUser.getId());
        return new ResponseEntity<>(new Response<>(UserJson.mapFromUser(user)), HttpStatus.OK);
    }
}
