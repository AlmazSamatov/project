package com.dds.web.controller.util;

import com.dds.common.exception.NotFoundException;
import com.dds.common.response.HttpResponseStatus;
import com.dds.common.response.Response;
import com.dds.service.exception.ForbiddenException;
import com.dds.service.exception.UnacceptableException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindingResult;

import static com.dds.common.response.Response.Error.Detail.getErrorDetails;

public class CommonExceptionResponses {
    public static <T> ResponseEntity<Response<T>> notFoundError(NotFoundException e) {
        return new ResponseEntity<>(new Response<>(null, Response.Error
                .builder()
                .type(HttpResponseStatus.NOT_FOUND)
                .message(e.getMessage())
                .build()
        ), HttpStatus.NOT_FOUND);
    }

    public static <T> ResponseEntity<Response<T>> forbiddenError(ForbiddenException e) {
        return new ResponseEntity<>(new Response<>(null, Response.Error
                .builder()
                .type(HttpResponseStatus.FORBIDDEN)
                .message(e.getMessage())
                .build()
        ), HttpStatus.FORBIDDEN);
    }

    public static <T> ResponseEntity<Response<T>> validationError(BindingResult errors) {
        return new ResponseEntity<>(
                new Response<>(null, Response.Error
                        .builder()
                        .type(HttpResponseStatus.VALIDATION_ERROR)
                        .details(getErrorDetails(errors))
                        .build()
                ), HttpStatus.BAD_REQUEST);
    }

    public static <T> ResponseEntity<Response<T>> unacceptableError(UnacceptableException e) {
        return new ResponseEntity<>(
                new Response<>(null, Response.Error
                        .builder()
                        .type(HttpResponseStatus.NOT_ACCEPTABLE)
                        .message(e.getMessage())
                        .build()
                ), HttpStatus.NOT_ACCEPTABLE);
    }

    public static <T> ResponseEntity<Response<T>> badCredentialsError(BadCredentialsException e) {
        return new ResponseEntity<>(new Response<>(null, Response.Error
                .builder()
                .type(HttpResponseStatus.BAD_CREDENTIALS)
                .message(e.getMessage())
                .build()
        ), HttpStatus.UNAUTHORIZED);
    }
}
