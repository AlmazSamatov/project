package com.dds.web.controller.admin;

import com.dds.common.exception.NotFoundException;
import com.dds.common.response.Response;
import com.dds.model.model.GroupUser;
import com.dds.model.model.User;
import com.dds.model.model.UserGroupBonus;
import com.dds.service.dto.form.get.FilterForm;
import com.dds.service.dto.form.get.FilterUsersForm;
import com.dds.service.dto.json.*;
import com.dds.service.service.BonusService;
import com.dds.service.service.GroupService;
import com.dds.service.service.UserService;
import com.dds.web.controller.util.CommonExceptionResponses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = {"Admin.User"})
@RestController
@RequestMapping(AdminUserController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class AdminUserController {
    public static final String ROOT_URL = "/v1/admin/users";
    public static final String USER = "/{user_id}";
    public static final String GROUPS = USER + "/groups";
    public static final String BONUSES = USER + "/bonuses";

    private final UserService userService;
    private final GroupService groupService;
    private final BonusService bonusService;

    @ApiOperation("Get users")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<Response<PageJson<UserJson>>> getUsers(@Valid @ModelAttribute FilterUsersForm form,
                                                                 BindingResult errors) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        Page<User> users = userService.getAllUsers(form);
        PageJson<UserJson> pageJson = new PageJson<>(
                users,
                users.map(UserJson::mapFromUser).getContent());
        return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);
    }

    @ApiOperation("Get user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(USER)
    public ResponseEntity<Response<UserJson>> getUsers(@PathVariable("user_id") Long userId) {
        try {
            User user = userService.getUser(userId);
            return new ResponseEntity<>(new Response<>(UserJson.mapFromUser(user)), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Get user groups")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(GROUPS)
    public ResponseEntity<Response<PageJson<UserGroupJson>>> getUserGroups(@PathVariable("user_id") Long userId,
                                                                           @Valid @ModelAttribute FilterForm form,
                                                                           BindingResult errors) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        try {
            Page<GroupUser> groups = groupService.getGroupsOfUser(userId, form);
            PageJson<UserGroupJson> pageJson = new PageJson<>(groups,
                    groups.map(UserGroupJson::mapFromGroupUser).getContent());
            return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Get user bonuses")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(BONUSES)
    public ResponseEntity<Response<PageJson<GroupBonusJson>>> getUserBonuses(@PathVariable("user_id") Long userId,
                                                                             @Valid @ModelAttribute FilterForm form,
                                                                             BindingResult errors) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        try {
            Page<UserGroupBonus> bonuses = bonusService.getBonusesOfUser(userId, form);
            PageJson<GroupBonusJson> pageJson = new PageJson<>(bonuses,
                    bonuses.map(GroupBonusJson::mapFromUserGroupBonus).getContent());
            return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }
}
