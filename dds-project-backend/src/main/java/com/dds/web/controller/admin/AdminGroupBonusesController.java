package com.dds.web.controller.admin;

import com.dds.common.exception.NotFoundException;
import com.dds.common.response.Response;
import com.dds.model.model.UserGroupBonus;
import com.dds.service.dto.form.post.AddGroupBonusForm;
import com.dds.service.dto.json.GroupBonusJson;
import com.dds.service.exception.UnacceptableException;
import com.dds.service.service.BonusService;
import com.dds.web.controller.util.CommonExceptionResponses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = {"Admin.Group.Bonuses"})
@RestController
@RequestMapping(AdminGroupBonusesController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class AdminGroupBonusesController {
    public static final String ROOT_URL = "/v1/admin/group/bonuses";

    public static final String BONUS = "/{group_bonus_id}";
    public static final String USERS = BONUS + "/users";
    public static final String USER = USERS + "/{user_id}";

    private final BonusService bonusService;

    @ApiOperation("Get group bonuses of user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(USER)
    public ResponseEntity<Response<GroupBonusJson>> getGroupBonusOfUser(
            @PathVariable("group_bonus_id") Long groupBonusId,
            @PathVariable("user_id") Long userId) {

        try {
            UserGroupBonus userGroupBonus = bonusService.getGroupBonusOfUser(groupBonusId, userId);
            return new ResponseEntity<>(new Response<>(
                    GroupBonusJson.mapFromUserGroupBonus(userGroupBonus)), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        } catch (UnacceptableException e) {
            return CommonExceptionResponses.unacceptableError(e);
        }
    }

    @ApiOperation("Add group bonuses to user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @PutMapping(USER)
    public ResponseEntity<Response<GroupBonusJson>> addGroupBonusToUser(
            @PathVariable("group_bonus_id") Long groupBonusId,
            @PathVariable("user_id") Long userId,
            @Valid @RequestBody AddGroupBonusForm form,
            BindingResult errors) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        try {
            UserGroupBonus userGroupBonus = bonusService.giveGroupBonusToUser(groupBonusId, userId, form.getAmount());
            return new ResponseEntity<>(new Response<>(
                    GroupBonusJson.mapFromUserGroupBonus(userGroupBonus)), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        } catch (UnacceptableException e) {
            return CommonExceptionResponses.unacceptableError(e);
        }
    }

    @ApiOperation("Remove group bonuses from user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @DeleteMapping(USER)
    public ResponseEntity<Response<GroupBonusJson>> removeGroupBonusesFromUser(
            @PathVariable("group_bonus_id") Long groupBonusId,
            @PathVariable("user_id") Long userId,
            @Valid @RequestBody AddGroupBonusForm form,
            BindingResult errors) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        try {
            UserGroupBonus userGroupBonus = bonusService.removeGroupBonusFromUser(
                    groupBonusId, userId, form.getAmount());
            return new ResponseEntity<>(new Response<>(
                    GroupBonusJson.mapFromUserGroupBonus(userGroupBonus)), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }
}
