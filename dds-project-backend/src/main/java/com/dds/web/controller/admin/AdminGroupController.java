package com.dds.web.controller.admin;

import com.dds.common.exception.NotFoundException;
import com.dds.common.response.Response;
import com.dds.model.model.Group;
import com.dds.model.model.GroupBonus;
import com.dds.model.model.GroupUser;
import com.dds.service.dto.form.get.FilterForm;
import com.dds.service.dto.form.post.AddGroupBonusForm;
import com.dds.service.dto.form.post.CreateGroupForm;
import com.dds.service.dto.json.GroupBonusJson;
import com.dds.service.dto.json.GroupJson;
import com.dds.service.dto.json.GroupUserJson;
import com.dds.service.dto.json.PageJson;
import com.dds.service.service.BonusService;
import com.dds.service.service.GroupService;
import com.dds.web.controller.util.CommonExceptionResponses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = {"Admin.Group"})
@RestController
@RequestMapping(AdminGroupController.ROOT_URL)
@Slf4j
@RequiredArgsConstructor
public class AdminGroupController {
    public static final String ROOT_URL = "/v1/admin/groups";
    public static final String GROUP = "/{group_id}";
    public static final String USERS = GROUP + "/users";
    public static final String USER = USERS + "/{user_id}";
    public static final String MODIFY_MODERATOR = USER + "/moderator";
    public static final String BONUSES = GROUP + "/bonuses";
    public static final String BONUS = BONUSES + "/{bonus_id}";

    private final GroupService groupService;
    private final BonusService bonusService;

    @ApiOperation("Get group")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(GROUP)
    public ResponseEntity<Response<GroupJson>> getGroup(@PathVariable("group_id") Long groupId) {
        try {
            Group group = groupService.getGroup(groupId);
            return new ResponseEntity<>(new Response<>(GroupJson.mapFromGroup(group, true)), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Get groups")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<Response<PageJson<GroupJson>>> getGroups(@Valid @ModelAttribute FilterForm form,
                                                                   BindingResult errors) {
        if (errors.hasErrors()) {
            return CommonExceptionResponses.validationError(errors);
        }

        Page<Group> groups = groupService.getGroups(form);
        PageJson<GroupJson> pageJson = new PageJson<>(groups,
                groups.map(x -> GroupJson.mapFromGroup(x, true)).getContent());
        return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);
    }

    @ApiOperation("Create group")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @PostMapping
    public ResponseEntity<Response<GroupJson>> createGroup(@Valid @RequestBody CreateGroupForm form,
                                                           BindingResult errors) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        Group group = groupService.createGroup(form);
        return new ResponseEntity<>(new Response<>(GroupJson.mapFromGroup(group, true)), HttpStatus.OK);
    }

    @ApiOperation("Delete group")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @DeleteMapping(GROUP)
    public ResponseEntity<Response<String>> deleteGroup(@PathVariable("group_id") Long groupId) {
        try {
            groupService.removeGroup(groupId);
            return new ResponseEntity<>(new Response<>("Ok"), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }


    @ApiOperation("Get group users")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(USERS)
    public ResponseEntity<Response<PageJson<GroupUserJson>>> getGroupUsers(@PathVariable("group_id") Long groupId,
                                                                           @Valid @ModelAttribute FilterForm form,
                                                                           BindingResult errors) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        try {
        Page<GroupUser> users = groupService.getUsersOfGroup(groupId, form);
        PageJson<GroupUserJson> pageJson = new PageJson<>(users,
                users.map(GroupUserJson::mapFromGroupUser).getContent());
        return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);} catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Accept user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @PostMapping(USER)
    public ResponseEntity<Response<String>> acceptUser(@PathVariable("group_id") Long groupId,
                                                       @PathVariable("user_id") Long userId) {
        try {
            groupService.acceptUser(groupId, userId);
            return new ResponseEntity<>(new Response<>("Ok"), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Set user as moderator")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @PostMapping(MODIFY_MODERATOR)
    public ResponseEntity<Response<String>> setUserAsModerator(@PathVariable("group_id") Long groupId,
                                                               @PathVariable("user_id") Long userId) {
        try {
            groupService.setModerator(groupId, userId);
            return new ResponseEntity<>(new Response<>("Ok"), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Unset moderator")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @DeleteMapping(MODIFY_MODERATOR)
    public ResponseEntity<Response<String>> unsetModerator(@PathVariable("group_id") Long groupId,
                                                           @PathVariable("user_id") Long userId) {
        try {
            groupService.unsetModerator(groupId, userId);
            return new ResponseEntity<>(new Response<>("Ok"), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Remove user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @DeleteMapping(USER)
    public ResponseEntity<Response<String>> removeUser(@PathVariable("group_id") Long groupId,
                                                       @PathVariable("user_id") Long userId) {
        try {
            groupService.removeUser(groupId, userId);
            return new ResponseEntity<>(new Response<>("Ok"), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Add bonus to group")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @PutMapping(BONUS)
    public ResponseEntity<Response<GroupBonusJson>> addBonus(@PathVariable("group_id") Long groupId,
                                                             @PathVariable("bonus_id") Long bonusId,
                                                             @Valid @RequestBody AddGroupBonusForm form,
                                                             BindingResult errors) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        try {
            GroupBonus groupBonus = bonusService.addGroupBonus(groupId, bonusId, form);
            return new ResponseEntity<>(new Response<>(
                    GroupBonusJson.mapFromGroupBonus(groupBonus)), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }

    @ApiOperation("Get all group bonuses")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header",
                    defaultValue = "%JWTTOKEN%", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping(BONUSES)
    public ResponseEntity<Response<PageJson<GroupBonusJson>>> getBonuses(@PathVariable("group_id") Long groupId,
                                                                         @Valid @ModelAttribute FilterForm form,
                                                                         BindingResult errors) {
        if (errors.hasErrors())
            return CommonExceptionResponses.validationError(errors);

        try {
            Page<GroupBonus> groupBonuses = bonusService.getGroupBonuses(groupId, form);
            PageJson<GroupBonusJson> pageJson = new PageJson<>(
                    groupBonuses,
                    groupBonuses.map(GroupBonusJson::mapFromGroupBonus).getContent()
            );
            return new ResponseEntity<>(new Response<>(pageJson), HttpStatus.OK);
        } catch (NotFoundException e) {
            return CommonExceptionResponses.notFoundError(e);
        }
    }
}
