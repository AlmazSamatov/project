package com.dds.web.config;

import com.dds.auth.Custom401AuthEntryPoint;
import com.dds.auth.CustomBasicAuthFilter;
import com.dds.auth.HeaderAuthenticationFilter;
import com.dds.auth.provider.UserPasswordAuthProvider;
import com.dds.model.model.GroupUser;
import com.dds.model.model.enums.GroupUserRole;
import com.dds.model.model.enums.UserRole;
import com.dds.service.service.AuthTokenService;
import com.dds.service.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserService userService;
    private final AuthTokenService authTokenService;
    private final UserPasswordAuthProvider userPasswordAuthProvider;
    private final BasicAuthenticationEntryPoint basicAuthEntryPoint;


    private static final String[] DOCS_ANT_PATTERNS = {
            "/csrf",
            "/v2/api-docs",
            "/**/swagger-ui.html",
            "/webjars/**",
            "/swagger-resources/**",
            "/api-docs"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        HeaderAuthenticationFilter filter = new HeaderAuthenticationFilter();
        filter.setAuthTokenService(authTokenService);
        filter.setUserService(userService);
        http
                .cors()
                .and()
                .authorizeRequests().antMatchers(DOCS_ANT_PATTERNS).hasAuthority("SWAGGER")
                .and().addFilterAt(new CustomBasicAuthFilter(authenticationManagerBean(), basicAuthEntryPoint, DOCS_ANT_PATTERNS), BasicAuthenticationFilter.class)
                .csrf().disable()
                .requestCache().disable()
                .rememberMe().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(filter)
                .authorizeRequests()
                .antMatchers("/**.js", "/**.html").permitAll()
                .antMatchers("/v1/user/**").hasAnyAuthority(UserRole.USER.name(), UserRole.ADMIN.name())
                .antMatchers("/v1/moderator/**").hasAnyAuthority(UserRole.USER.name(), UserRole.ADMIN.name())
                .antMatchers("/v1/admin/**").hasAuthority(UserRole.ADMIN.name())
                .antMatchers("/v1/**").permitAll()
                .and()
                .exceptionHandling().authenticationEntryPoint(new Custom401AuthEntryPoint(DOCS_ANT_PATTERNS, "Basic"))
                .accessDeniedHandler(new MyAccessDeniedHandlerImpl())
                .and()
                .logout().permitAll();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowedMethods(
                Arrays.asList(
                        HttpMethod.GET.name(),
                        HttpMethod.HEAD.name(),
                        HttpMethod.POST.name(),
                        HttpMethod.DELETE.name(),
                        HttpMethod.PUT.name()
                )
        );
        source.registerCorsConfiguration("/**", corsConfiguration.applyPermitDefaultValues());
        return source;
    }

    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(userPasswordAuthProvider);
    }

    public class MyAccessDeniedHandlerImpl implements AccessDeniedHandler {
        @Override
        public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
            response.setStatus(HttpStatus.FORBIDDEN.value());
            PrintWriter writer = response.getWriter();
            writer.write("{ \"result\": null, \"error\": " +
                    "{ \"message\": \"You haven't authority to access this page\", \"type\": \"access-denied\" } }");
            writer.flush();
            writer.close();
        }
    }
}
