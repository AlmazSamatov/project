package com.dds.service.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;

public class PasswordUtil {
    public static final int DEFAULT_PASSWORD_LENGTH = 10;

    public static String generate() {
        return generate(DEFAULT_PASSWORD_LENGTH);
    }

    public static String generate(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }

    public static String getHash(String password) {
        return DigestUtils.sha256Hex(password);
    }
}
