package com.dds.service.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Pair<A, B> implements Comparable<Pair<A, B>> {
    private A first;
    private B second;

    @Override
    public int compareTo(Pair<A, B> o) {
        if (first == null && second == null) {
            return 0;
        }
        if (first == null) {
            return Integer.compare(second.hashCode(), o.second.hashCode());
        }
        if (second == null) {
            return Integer.compare(first.hashCode(), o.first.hashCode());
        }
        if (first.hashCode() < o.first.hashCode()){
            return -1;
        } else if (first.hashCode() > o.first.hashCode()) {
            return 1;
        }
        return Integer.compare(second.hashCode(), o.second.hashCode());
    }

    @Override
    public String toString() {
        return "Pair{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}
