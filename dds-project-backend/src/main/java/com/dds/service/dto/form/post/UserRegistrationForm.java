package com.dds.service.dto.form.post;

import com.dds.model.model.User;
import com.dds.model.model.enums.UserRole;
import com.dds.service.validator.ValidationConstants;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;

@Getter
@Setter
public class UserRegistrationForm {

    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String TELEGRAM_ALIAS = "telegram_alias";
    public static final String PASSWORD = "password";
    public static final String CONFIRM_PASSWORD = "confirm_password";

    @JsonProperty(value = NAME, required = true)
    @NotBlank(message = "The name must not be blank")
    private String name;

    @JsonProperty(value = EMAIL, required = true)
    @NotBlank(message = "The email must not be blank")
    @Email(message = "Email format is wrong")
    private String email;

    @JsonProperty(value = TELEGRAM_ALIAS)
    private String telegramAlias;

    @Pattern(regexp = ValidationConstants.PASSWORD_PATTERN,
            message = ValidationConstants.PASSWORD_VALIDATION_MESSAGE)
    @JsonProperty(value = PASSWORD, required = true)
    @NotBlank(message = "The password must not be blank")
    private String password;

    @JsonProperty(value = CONFIRM_PASSWORD, required = true)
    private String confirmPassword;

    public User toUser() {
        return User
                .builder()
                .name(name)
                .email(StringUtils.trim(email).toLowerCase())
                .telegramAlias(telegramAlias)
                .role(UserRole.USER)
                .build();
    }
}
