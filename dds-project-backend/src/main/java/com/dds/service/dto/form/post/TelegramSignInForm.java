package com.dds.service.dto.form.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

@Getter
@Setter
public class TelegramSignInForm {
    public static final String TELEGRAM_BOT_TOKEN = "telegram_bot_token";
    public static final String TELEGRAM_ALIAS = "telegram_alias";

    @NotBlank(message = "The telegram-bot-token must not be empty")
    @JsonProperty(value = TELEGRAM_BOT_TOKEN)
    private String telegramBotToken;

    @NotBlank(message = "The telegram alias must not be empty")
    @JsonProperty(value = TELEGRAM_ALIAS, required = true)
    private String telegramAlias;
}
