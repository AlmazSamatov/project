package com.dds.service.dto.json;

import com.dds.model.model.GroupUser;
import com.dds.model.model.enums.GroupUserRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserGroupJson {
    private GroupJson group;

    private GroupUserRole role;

    public static UserGroupJson mapFromGroupUser(GroupUser groupUser) {
        return UserGroupJson.builder()
                .group(GroupJson.mapFromGroup(groupUser.getGroup(), false))
                .role(groupUser.getRole())
                .build();
    }
}
