package com.dds.service.dto.json;

import com.dds.model.model.GroupBonus;
import com.dds.model.model.UserGroupBonus;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class GroupBonusJson {

    @JsonProperty("group_bonus_id")
    private Long groupBonusId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private GroupJson group;

    private BonusJson bonus;

    private Integer amount;

    public static GroupBonusJson mapFromGroupBonus(GroupBonus groupBonus) {
        return GroupBonusJson.builder()
                .groupBonusId(groupBonus.getId())
                .bonus(BonusJson.mapFromBonus(groupBonus.getBonus()))
                .amount(groupBonus.getRemainingAmount())
                .build();
    }

    public static GroupBonusJson mapFromUserGroupBonus(UserGroupBonus userGroupBonus) {
        GroupBonus groupBonus = userGroupBonus.getGroupBonus();
        return GroupBonusJson.builder()
                .groupBonusId(groupBonus.getId())
                .group(GroupJson.mapFromGroup(groupBonus.getGroup(), false))
                .bonus(BonusJson.mapFromBonus(groupBonus.getBonus()))
                .amount(userGroupBonus.getAmount())
                .build();
    }
}
