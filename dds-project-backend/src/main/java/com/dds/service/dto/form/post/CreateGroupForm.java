package com.dds.service.dto.form.post;

import com.dds.model.model.Group;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

@Getter
@Setter
public class CreateGroupForm {
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";

    @JsonProperty(value = TITLE, required = true)
    @NotBlank(message = "The title must not be blank")
    private String title;

    @JsonProperty(value = DESCRIPTION)
    private String description;

    @JsonProperty(value = "is_visible")
    private Boolean isVisible = true;

    public Group toGroup() {
        return Group.builder()
                .title(title)
                .description(description)
                .isVisible(isVisible)
                .build();
    }
}
