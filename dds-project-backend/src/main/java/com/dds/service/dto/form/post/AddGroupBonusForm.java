package com.dds.service.dto.form.post;

import com.dds.model.model.GroupBonus;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AddGroupBonusForm {
    public static final String AMOUNT = "amount";

    @JsonProperty(value = AMOUNT, required = true)
    @NotNull(message = "The amount must not be null")
    @Range(message = "The amount must not be negative")
    private Integer amount;

    public GroupBonus toGroupBonus() {
        return GroupBonus.builder()
                .remainingAmount(amount)
                .build();
    }
}
