package com.dds.service.dto.json;

import com.dds.model.model.Bonus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BonusJson {

    private Long id;

    private String title;

    private String description;

    private Boolean isCountable;

    public static BonusJson mapFromBonus(Bonus bonus) {
        return BonusJson.builder()
                .id(bonus.getId())
                .title(bonus.getTitle())
                .description(bonus.getDescription())
                .isCountable(bonus.getIsCountable())
                .build();
    }
}
