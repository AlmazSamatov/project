package com.dds.service.dto.json;

import com.dds.model.model.User;
import com.dds.model.model.enums.UserRole;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserJson {
    private Long id;

    private String email;

    private String name;

    @JsonProperty("telegram_alias")
    private String telegramAlias;

    private UserRole role;

    public static UserJson mapFromUser(User user) {
        return UserJson.builder()
                .id(user.getId())
                .email(user.getEmail())
                .name(user.getName())
                .telegramAlias(user.getTelegramAlias())
                .role(user.getRole())
                .build();
    }
}
