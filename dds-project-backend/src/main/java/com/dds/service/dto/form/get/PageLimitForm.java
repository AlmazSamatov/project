package com.dds.service.dto.form.get;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.domain.PageRequest;

@Getter
@Setter
public class PageLimitForm {
    public static final String LIMIT = "limit";
    public static final String PAGE = "offset";

    @Range(min = 0, max = Integer.MAX_VALUE, message = "The limit must not be negative")
    @ApiModelProperty(value = LIMIT)
    private Integer limit = 20;

    @Range(min = 0, max = Integer.MAX_VALUE, message = "The page must not be negative")
    @ApiModelProperty(value = PAGE)
    private Integer page = 0;

    public PageRequest toPageRequest() {
        return new PageRequest(page, limit);
    }
}
