package com.dds.service.dto.form.post;

import com.dds.model.model.Bonus;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;


@Getter
@Setter
public class CreateBonusForm {
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String IS_COUNTABLE = "is_countable";

    @JsonProperty(value = TITLE, required = true)
    @NotBlank(message = "The title must not be blank")
    private String title;

    @JsonProperty(value = DESCRIPTION)
    private String description;

    @JsonProperty(value = IS_COUNTABLE)
    private Boolean isCountable = true;

    public Bonus toBonus() {
        return Bonus.builder()
                .title(title)
                .description(description)
                .isCountable(isCountable)
                .build();
    }
}
