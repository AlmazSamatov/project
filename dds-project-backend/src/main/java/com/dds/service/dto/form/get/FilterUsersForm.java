package com.dds.service.dto.form.get;

import com.dds.model.model.User;
import com.dds.model.util.Sort;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilterUsersForm extends SortFilterForm {
    public static final String SORT = "sort";

    @ApiModelProperty(value = SORT)
    private User.UsersSort sort = User.UsersSort.NAME;

    public Sort getSort() {
        return sort;
    }
}
