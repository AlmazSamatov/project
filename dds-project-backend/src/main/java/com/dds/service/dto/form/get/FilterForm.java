package com.dds.service.dto.form.get;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilterForm extends PageLimitForm {
    public static final String QUERY = "query";

    @ApiModelProperty(value = QUERY)
    private String query = "";
}
