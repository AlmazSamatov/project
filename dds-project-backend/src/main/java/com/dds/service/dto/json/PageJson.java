package com.dds.service.dto.json;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Getter
@NoArgsConstructor
public class PageJson<T> {
    private List<T> content;

    private Integer page;

    private Integer size;

    private Long total;

    public PageJson(Integer page, Integer total, List<T> content) {
        this.content = content;
        this.page = page;
        this.size = content.size();
        this.total = total.longValue();
    }

    public PageJson(Page page, List<T> content) {
        setPage(page);
        setContent(content);
    }

    public void setPage(Page page) {
        this.page = page.getNumber();
        this.size = page.getNumberOfElements();
        this.total = page.getTotalElements();
    }

    public void setContent(List<T> content) {
        this.content = content;
    }
}
