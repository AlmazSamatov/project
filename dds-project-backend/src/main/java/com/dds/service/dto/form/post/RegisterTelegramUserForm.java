package com.dds.service.dto.form.post;

import com.dds.model.model.User;
import com.dds.model.model.enums.UserRole;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;

@Getter
@Setter
public class RegisterTelegramUserForm {
    public static final String TELEGRAM_BOT_TOKEN = "telegram_bot_token";
    public static final String NAME = "name";
    public static final String TELEGRAM_ALIAS = "telegram_alias";

    @NotBlank(message = "The telegram-bot-token must not be empty")
    @JsonProperty(value = TELEGRAM_BOT_TOKEN)
    private String telegramBotToken;

    @JsonProperty(value = NAME, required = true)
    @NotBlank(message = "The name must not be blank")
    private String name;

    @JsonProperty(value = TELEGRAM_ALIAS)
    @NotBlank(message = "The telegram alias must not be blank")
    private String telegramAlias;

    public User toUser() {
        return User
                .builder()
                .name(name)
                .telegramAlias(StringUtils.trim(telegramAlias).toLowerCase())
                .role(UserRole.USER)
                .build();
    }
}
