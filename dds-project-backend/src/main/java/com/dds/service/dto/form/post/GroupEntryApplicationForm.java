package com.dds.service.dto.form.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GroupEntryApplicationForm {
    public static final String GROUP_ID = "group_id";

    @NotBlank(message = "The id/invitation_code must not be blank")
    @JsonProperty(value = GROUP_ID, required = true)
    private String id;
}
