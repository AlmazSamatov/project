package com.dds.service.dto.json;

import com.dds.model.model.GroupUser;
import com.dds.model.model.enums.GroupUserRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class GroupUserJson {

    private UserJson user;

    private GroupUserRole role;

    public static GroupUserJson mapFromGroupUser(GroupUser groupUser) {
        return GroupUserJson.builder()
                .user(UserJson.mapFromUser(groupUser.getUser()))
                .role(groupUser.getRole())
                .build();
    }
}
