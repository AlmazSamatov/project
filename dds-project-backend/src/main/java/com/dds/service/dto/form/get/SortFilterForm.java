package com.dds.service.dto.form.get;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Getter
@Setter
public class SortFilterForm extends FilterForm {
    public static final String SORT_DIRECTION = "sort_direction";

    @ApiModelProperty(value = SORT_DIRECTION, name = SORT_DIRECTION)
    private Sort.Direction sortDirection = Sort.Direction.ASC;

    public com.dds.model.util.Sort getSort() {
        return null;
    }

    public PageRequest toPageRequest() {
        String sort;
        if (getSort() != null) {
            sort = getSort().getField();
        } else {
            sort = "id";
        }
        return new PageRequest(getPage(), getLimit(),
                getSortDirection(), sort);
    }
}
