package com.dds.service.dto.json;

import com.dds.model.model.Group;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class GroupJson {
    private Long id;

    private String title;

    private String description;

    @JsonProperty("is_visible")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean isVisible;

    @JsonProperty("invitation_code")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String invitationCode;

    public static GroupJson mapFromGroup(Group group, boolean showVisibility) {
        return GroupJson.builder()
                .id(group.getId())
                .title(group.getTitle())
                .description(group.getDescription())
                .isVisible(showVisibility ? group.getIsVisible() : null)
                .invitationCode(showVisibility ? group.getInvitationCode() : null)
                .build();
    }
}
