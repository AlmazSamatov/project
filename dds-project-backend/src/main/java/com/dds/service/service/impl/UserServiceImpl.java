package com.dds.service.service.impl;

import com.dds.common.exception.NotFoundException;
import com.dds.model.model.User;
import com.dds.model.repository.UserRepository;
import com.dds.service.dto.form.get.FilterUsersForm;
import com.dds.service.dto.form.post.RegisterTelegramUserForm;
import com.dds.service.dto.form.post.UserRegistrationForm;
import com.dds.service.exception.DuplicateException;
import com.dds.service.service.UserService;
import com.dds.service.util.PasswordUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Transactional
    @Override
    public void registerUser(UserRegistrationForm form) {
        User user = form.toUser();

        if (userRepository.getByEmail(user.getEmail()).isPresent()) {
            throw new DuplicateException("Duplicate email");
        }
        if (!Strings.isBlank(user.getTelegramAlias()) &&
                userRepository.getByTelegramAlias(user.getTelegramAlias()).isPresent()) {
            throw new DuplicateException("Duplicate telegram alias");
        }
        user.setPasswordHash(PasswordUtil.getHash(form.getPassword()));
        userRepository.save(user);
    }

    @Transactional
    @Override
    public void registerTelegramUser(RegisterTelegramUserForm form) {
        User user = form.toUser();
        if (userRepository.getByTelegramAlias(user.getTelegramAlias()).isPresent()) {
            throw new DuplicateException("Duplicate telegram alias");
        }
        userRepository.save(user);
    }

    @Override
    public User getUser(Long userId) {
        return userRepository.getById(userId).orElseThrow(
                () -> new NotFoundException(String.format("User %d not found", userId))
        );
    }

    @Override
    public User getUserByEmailOrTelegramAlias(String login) {
        return userRepository.getByEmailOrTelegramAlias(login, login).orElseThrow(
                () -> new NotFoundException(String.format("User %s not found", login))
        );
    }

    @Override
    public Page<User> getAllUsers(FilterUsersForm form) {
        return userRepository.getAllByEmailContainingOrTelegramAliasContaining(
                form.getQuery(), form.getQuery(),
                form.toPageRequest());
    }

    @Override
    public User submitUser(User user) {
        return userRepository.save(user);
    }
}
