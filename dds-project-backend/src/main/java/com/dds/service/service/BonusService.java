package com.dds.service.service;

import com.dds.model.model.Bonus;
import com.dds.model.model.GroupBonus;
import com.dds.model.model.UserGroupBonus;
import com.dds.service.dto.form.get.FilterForm;
import com.dds.service.dto.form.post.CreateBonusForm;
import com.dds.service.dto.form.post.AddGroupBonusForm;
import org.springframework.data.domain.Page;

public interface BonusService {

    Bonus getBonus(Long id);

    Bonus createBonus(CreateBonusForm form);

    Page<Bonus> getBonuses(FilterForm form);

    GroupBonus getGroupBonus(Long id);

    void deleteBonus(Long id);

    GroupBonus addGroupBonus(Long groupId, Long bonusId, AddGroupBonusForm form);

    Page<GroupBonus> getGroupBonuses(Long groupId, FilterForm form);

    UserGroupBonus giveGroupBonusToUser(Long groupBonusId, Long userId, Integer amount);

    UserGroupBonus removeGroupBonusFromUser(Long groupBonusId, Long userId, Integer amount);

    Page<UserGroupBonus> getBonusesOfUser(Long userId, FilterForm form);

    UserGroupBonus getGroupBonusOfUser(Long groupBonusId, Long userId);
}
