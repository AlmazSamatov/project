package com.dds.service.service;


public interface AuthTokenService {

    String generateToken(Long userId);

    Long getUserId(String authToken);
}
