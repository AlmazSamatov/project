package com.dds.service.service;

import com.dds.model.model.Group;
import com.dds.model.model.GroupUser;
import com.dds.service.dto.form.get.FilterForm;
import com.dds.service.dto.form.post.CreateGroupForm;
import org.springframework.data.domain.Page;

public interface GroupService {

    Group createGroup(CreateGroupForm form);

    Group getGroup(Long id);

    Page<GroupUser> getUsersOfGroup(Long id, FilterForm form);

    Page<Group> getGroups(FilterForm form);

    Group getVisibleGroup(Long id);

    Page<Group> getVisibleGroups(FilterForm form);

    Page<GroupUser> getGroupsOfUser(Long userId, FilterForm form);

    GroupUser getGroupOfUser(Long id, Long userId);

    Page<Group> getGroupsOfModerator(Long userId, FilterForm form);

    void removeGroup(Long groupId);

    void addPendingUser(String groupId, Long userId);

    void acceptUser(Long groupId, Long userId);

    void removeUser(Long groupId, Long userId);

    void setModerator(Long groupId, Long userId);

    void unsetModerator(Long groupId, Long userId);

    void checkOnModerator(Long groupId, Long userId);
}
