package com.dds.service.service.impl;

import com.dds.common.exception.NotFoundException;
import com.dds.model.model.Bonus;
import com.dds.model.model.GroupBonus;
import com.dds.model.model.User;
import com.dds.model.model.UserGroupBonus;
import com.dds.model.repository.BonusRepository;
import com.dds.model.repository.GroupBonusRepository;
import com.dds.model.repository.UserGroupBonusRepository;
import com.dds.service.dto.form.get.FilterForm;
import com.dds.service.dto.form.post.CreateBonusForm;
import com.dds.service.dto.form.post.AddGroupBonusForm;
import com.dds.service.exception.UnacceptableException;
import com.dds.service.service.BonusService;
import com.dds.service.service.GroupService;
import com.dds.service.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class BonusServiceImpl implements BonusService {

    private final BonusRepository bonusRepository;
    private final GroupBonusRepository groupBonusRepository;
    private final UserGroupBonusRepository userGroupBonusRepository;
    private final GroupService groupService;
    private final UserService userService;

    @Override
    public Bonus getBonus(Long id) {
        return bonusRepository.getById(id).orElseThrow(
                () -> new NotFoundException(String.format("Bonus %d not found", id))
        );
    }

    @Override
    public Bonus createBonus(CreateBonusForm form) {
        Bonus bonus = form.toBonus();
        return bonusRepository.save(bonus);
    }

    @Override
    public Page<Bonus> getBonuses(FilterForm form) {
        return bonusRepository.getAllByTitleContainingIgnoreCase(form.getQuery(),
                form.toPageRequest());
    }

    @Override
    public GroupBonus getGroupBonus(Long id) {
        return groupBonusRepository.getById(id).orElseThrow(
                () -> new NotFoundException(String.format("Group bonus %d not found", id))
        );
    }

    @Override
    public void deleteBonus(Long id) {
        try {
            bonusRepository.delete(id);
        } catch (EmptyResultDataAccessException e) {
            throw new NotFoundException(String.format("Bonus %d not found", id));
        }

    }

    @Transactional
    @Override
    public GroupBonus addGroupBonus(Long groupId, Long bonusId, AddGroupBonusForm form) {
        GroupBonus groupBonus;
        try {
            groupBonus = getGroupBonus(groupId, bonusId);
            groupBonus.setRemainingAmount(groupBonus.getRemainingAmount() + form.getAmount());
        } catch (NotFoundException e) {
            groupBonus = form.toGroupBonus();
            groupBonus.setBonus(getBonus(bonusId));
            groupBonus.setGroup(groupService.getGroup(groupId));
        }
        return groupBonusRepository.save(groupBonus);
    }

    @Override
    public Page<GroupBonus> getGroupBonuses(Long groupId, FilterForm form) {
        groupService.getGroup(groupId);
        return groupBonusRepository.getAllByGroupId(groupId, form.toPageRequest());
    }

    @Transactional
    @Override
    public UserGroupBonus giveGroupBonusToUser(Long groupBonusId, Long userId, Integer amount) {
        GroupBonus groupBonus = getGroupBonus(groupBonusId);
        if (groupBonus.getRemainingAmount() < amount) {
            throw new UnacceptableException("Remaining amount of bonuses is less than required");
        }
        groupBonus.setRemainingAmount(groupBonus.getRemainingAmount() - amount);
        User user = groupService.getGroupOfUser(groupBonus.getGroup().getId(), userId).getUser();
        UserGroupBonus userGroupBonus =
                userGroupBonusRepository
                        .getByGroupBonusIdAndUserId(groupBonus.getId(), userId)
                        .orElse(UserGroupBonus.builder()
                                .amount(0)
                                .groupBonus(groupBonus)
                                .user(user)
                                .build());
        userGroupBonus.setAmount(userGroupBonus.getAmount() + amount);
        userGroupBonus.setGroupBonus(groupBonus);
        return userGroupBonusRepository.save(userGroupBonus);
    }

    @Transactional
    @Override
    public UserGroupBonus removeGroupBonusFromUser(Long groupBonusId, Long userId, Integer amount) {
        UserGroupBonus userGroupBonus = getGroupBonusOfUser(groupBonusId, userId);
        if (userGroupBonus.getAmount() <= amount ||
                !userGroupBonus.getGroupBonus().getBonus().getIsCountable()) {
            userGroupBonus.setAmount(0);
            userGroupBonusRepository.delete(userGroupBonus);
            return userGroupBonus;
        }
        userGroupBonus.setAmount(userGroupBonus.getAmount() - amount);
        return userGroupBonusRepository.save(userGroupBonus);
    }

    @Override
    public Page<UserGroupBonus> getBonusesOfUser(Long userId, FilterForm form) {
        userService.getUser(userId);
        return userGroupBonusRepository.getAllByUserId(userId, form.toPageRequest());
    }

    @Override
    public UserGroupBonus getGroupBonusOfUser(Long groupBonusId, Long userId) {
        return userGroupBonusRepository.getByGroupBonusIdAndUserId(groupBonusId, userId).orElseThrow(
                () -> new NotFoundException(
                        String.format("Group bonus %d of user %d not found", groupBonusId, userId))
        );
    }

    private GroupBonus getGroupBonus(Long groupId, Long bonusId) {
        return groupBonusRepository.getByGroupIdAndBonusId(groupId, bonusId).orElseThrow(
                () -> new NotFoundException("Group bonus not found")
        );
    }
}
