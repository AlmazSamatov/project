package com.dds.service.service;

import com.dds.model.model.User;
import com.dds.service.dto.form.get.FilterUsersForm;
import com.dds.service.dto.form.post.RegisterTelegramUserForm;
import com.dds.service.dto.form.post.UserRegistrationForm;
import org.springframework.data.domain.Page;

public interface UserService {
    void registerUser(UserRegistrationForm userRegistrationForm);

    void registerTelegramUser(RegisterTelegramUserForm form);

    User getUser(Long userId);

    User getUserByEmailOrTelegramAlias(String login);

    Page<User> getAllUsers(FilterUsersForm form);

    User submitUser(User user);
}
