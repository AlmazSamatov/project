package com.dds.service.service.impl;

import com.dds.common.exception.NotFoundException;
import com.dds.model.model.Group;
import com.dds.model.model.GroupUser;
import com.dds.model.model.User;
import com.dds.model.model.enums.GroupUserRole;
import com.dds.model.repository.GroupRepository;
import com.dds.model.repository.GroupUserRepository;
import com.dds.service.dto.form.get.FilterForm;
import com.dds.service.dto.form.post.CreateGroupForm;
import com.dds.service.exception.DuplicateException;
import com.dds.service.exception.ForbiddenException;
import com.dds.service.service.GroupService;
import com.dds.service.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;
    private final GroupUserRepository groupUserRepository;
    private final UserService userService;


    @Override
    public Group createGroup(CreateGroupForm form) {
        Group group = form.toGroup();

        String code = RandomStringUtils.randomAlphanumeric(10);
        while (groupRepository.getByInvitationCode(code).isPresent()) {
            code = RandomStringUtils.randomAlphanumeric(10);
        }
        group.setInvitationCode(code);

        return groupRepository.save(group);
    }

    @Override
    public Group getGroup(Long id) {
        return groupRepository.getById(id).orElseThrow(
                () -> new NotFoundException(String.format("Group %d not found", id))
        );
    }

    private Group getByInvitationCode(String code) {
        return groupRepository.getByInvitationCode(code).orElseThrow(
                () -> new NotFoundException(String.format("Group %s not found", code))
        );
    }

    @Override
    public Page<GroupUser> getUsersOfGroup(Long id, FilterForm form) {
        getGroup(id);
        return groupUserRepository
                .getAllByGroupId(id, form.toPageRequest());
    }

    @Override
    public Page<Group> getGroups(FilterForm form) {
        return groupRepository.getAllByTitleContainingIgnoreCase(form.getQuery(),
                form.toPageRequest());
    }

    @Override
    public Group getVisibleGroup(Long id) {
        Group group = getGroup(id);
        if (group.getInvitationCode() == null) return group;
        throw new NotFoundException(String.format("Group %d not found", id));
    }

    @Override
    public Page<Group> getVisibleGroups(FilterForm form) {
        return groupRepository.getAllByTitleContainingIgnoreCaseAndIsVisible(form.getQuery(), true,
                form.toPageRequest());
    }

    @Override
    public Page<GroupUser> getGroupsOfUser(Long userId, FilterForm form) {
        userService.getUser(userId);
        return groupUserRepository
                .getAllByUserIdAndRoleIn(userId, Arrays.asList(GroupUserRole.USER,
                        GroupUserRole.MODERATOR), form.toPageRequest());
    }

    @Override
    public GroupUser getGroupOfUser(Long id, Long userId) {
        return getGroupUser(id, userId, null);
    }

    @Override
    public Page<Group> getGroupsOfModerator(Long userId, FilterForm form) {
        userService.getUser(userId);
        return groupUserRepository
                .getAllByUserIdAndRoleIn(userId, Arrays.asList(GroupUserRole.MODERATOR), form.toPageRequest())
                .map(GroupUser::getGroup);
    }

    @Override
    public void removeGroup(Long groupId) {
        Group group = getGroup(groupId);
        groupRepository.delete(group);
    }

    @Override
    public void addPendingUser(String groupId, Long userId) {
        addPendingUser(groupId, userId, false);
    }

    private void addPendingUser(String groupId, Long userId, boolean internalCall) {
        User user = userService.getUser(userId);
        Group group;
        if (StringUtils.isNumeric(groupId)) {
            group = getGroup(Long.parseLong(groupId));
            if (!group.getIsVisible() && !internalCall)
                throw new NotFoundException(String.format("Group %s not found", groupId));
        } else {
            group = getByInvitationCode(groupId);
        }
        GroupUser groupUser = GroupUser.builder()
                .user(user)
                .group(group)
                .role(GroupUserRole.PENDING)
                .build();
        try {
            groupUserRepository.save(groupUser);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateException("You have already submitted application for entry.");
        }
    }

    @Override
    public void acceptUser(Long groupId, Long userId) {
        try {
            changeRole(groupId, userId, GroupUserRole.USER);
        } catch (NotFoundException e) {
            addPendingUser(String.valueOf(groupId), userId, true);
            changeRole(groupId, userId, GroupUserRole.USER);
        }
    }

    @Override
    public void removeUser(Long groupId, Long userId) {
        GroupUser groupUser = getGroupUser(groupId, userId, null);
        groupUserRepository.delete(groupUser);
    }

    @Override
    public void setModerator(Long groupId, Long userId) {
        changeRole(groupId, userId, GroupUserRole.MODERATOR);
    }

    @Override
    public void unsetModerator(Long groupId, Long userId) {
        changeRole(groupId, userId, GroupUserRole.USER);
    }

    @Override
    public void checkOnModerator(Long groupId, Long userId) {
        getGroupUser(groupId, userId, GroupUserRole.MODERATOR);
    }

    private void changeRole(Long groupId, Long userId, GroupUserRole role) {
        GroupUser groupUser = getGroupUser(groupId, userId, null);
        groupUser.setRole(role);
        groupUserRepository.save(groupUser);
    }

    private GroupUser getGroupUser(Long groupId, Long userId, GroupUserRole role) {
        GroupUser user = groupUserRepository.getByGroupIdAndUserId(groupId, userId).orElseThrow(
                () -> new NotFoundException("User not in the group")
        );
        if (role == null || user.getRole().equals(role))
            return user;
        throw new ForbiddenException("Not enough rights to access group");
    }
}
