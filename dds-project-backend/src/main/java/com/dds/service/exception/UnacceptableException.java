package com.dds.service.exception;

public class UnacceptableException extends RuntimeException {
    public UnacceptableException(String message) {
        super(message);
    }
}
