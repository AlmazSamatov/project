package com.dds.service.exception;

public class AuthTokenParseException extends RuntimeException {
    public AuthTokenParseException(String message) {
        super(message);
    }
}
