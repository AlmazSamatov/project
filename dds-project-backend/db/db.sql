/*
Navicat PGSQL Data Transfer

Source Server         : Platform_local
Source Server Version : 110100
Source Host           : localhost:5432
Source Database       : postgres
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 110100
File Encoding         : 65001

Date: 2019-12-06 18:56:09
*/


-- ----------------------------
-- Table structure for "public"."bonuses"
-- ----------------------------
DROP TABLE "public"."bonuses";
CREATE TABLE "public"."bonuses" (
"id" int8 NOT NULL,
"created_date" timestamp(6),
"last_modified_date" timestamp(6),
"description" varchar(255),
"is_countable" bool,
"title" varchar(255)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of bonuses
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."group_bonus_users"
-- ----------------------------
DROP TABLE "public"."group_bonus_users";
CREATE TABLE "public"."group_bonus_users" (
"id" int8 NOT NULL,
"created_date" timestamp(6),
"last_modified_date" timestamp(6),
"amount" int4,
"group_bonus_id" int8,
"user_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of group_bonus_users
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."group_bonuses"
-- ----------------------------
DROP TABLE "public"."group_bonuses";
CREATE TABLE "public"."group_bonuses" (
"id" int8 NOT NULL,
"created_date" timestamp(6),
"last_modified_date" timestamp(6),
"remaining_amount" int4,
"bonus_id" int8,
"group_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of group_bonuses
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."group_users"
-- ----------------------------
DROP TABLE "public"."group_users";
CREATE TABLE "public"."group_users" (
"id" int8 NOT NULL,
"role" varchar(255),
"group_id" int8,
"user_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of group_users
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."groups"
-- ----------------------------
DROP TABLE "public"."groups";
CREATE TABLE "public"."groups" (
"id" int8 NOT NULL,
"created_date" timestamp(6),
"last_modified_date" timestamp(6),
"description" varchar(255),
"invitation_code" varchar(255),
"is_visible" bool,
"title" varchar(255)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of groups
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."hibernate_sequences"
-- ----------------------------
DROP TABLE "public"."hibernate_sequences";
CREATE TABLE "public"."hibernate_sequences" (
"sequence_name" varchar(255) NOT NULL,
"next_val" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of hibernate_sequences
-- ----------------------------
INSERT INTO "public"."hibernate_sequences" VALUES ('users', '2');

-- ----------------------------
-- Table structure for "public"."users"
-- ----------------------------
DROP TABLE "public"."users";
CREATE TABLE "public"."users" (
"id" int8 NOT NULL,
"created_date" timestamp(6),
"last_modified_date" timestamp(6),
"email" varchar(100),
"name" varchar(250),
"password_hash" varchar(512),
"role" varchar(255),
"telegram_alias" varchar(100)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES ('1', '2019-12-06 18:55:39.854', '2019-12-06 18:55:39.854', 'admin@gmail.com', 'admin', '96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e', 'ADMIN', null);

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table "public"."bonuses"
-- ----------------------------
ALTER TABLE "public"."bonuses" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table "public"."group_bonus_users"
-- ----------------------------
ALTER TABLE "public"."group_bonus_users" ADD UNIQUE ("group_bonus_id", "user_id");

-- ----------------------------
-- Primary Key structure for table "public"."group_bonus_users"
-- ----------------------------
ALTER TABLE "public"."group_bonus_users" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table "public"."group_bonuses"
-- ----------------------------
ALTER TABLE "public"."group_bonuses" ADD UNIQUE ("group_id", "bonus_id");

-- ----------------------------
-- Primary Key structure for table "public"."group_bonuses"
-- ----------------------------
ALTER TABLE "public"."group_bonuses" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table "public"."group_users"
-- ----------------------------
ALTER TABLE "public"."group_users" ADD UNIQUE ("group_id", "user_id");

-- ----------------------------
-- Primary Key structure for table "public"."group_users"
-- ----------------------------
ALTER TABLE "public"."group_users" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table groups
-- ----------------------------
CREATE INDEX "invitation_code_index" ON "public"."groups" USING btree ("invitation_code");

-- ----------------------------
-- Uniques structure for table "public"."groups"
-- ----------------------------
ALTER TABLE "public"."groups" ADD UNIQUE ("invitation_code");

-- ----------------------------
-- Primary Key structure for table "public"."groups"
-- ----------------------------
ALTER TABLE "public"."groups" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table "public"."hibernate_sequences"
-- ----------------------------
ALTER TABLE "public"."hibernate_sequences" ADD PRIMARY KEY ("sequence_name");

-- ----------------------------
-- Indexes structure for table users
-- ----------------------------
CREATE INDEX "alias_index" ON "public"."users" USING btree ("telegram_alias");
CREATE INDEX "email_index" ON "public"."users" USING btree ("email");

-- ----------------------------
-- Uniques structure for table "public"."users"
-- ----------------------------
ALTER TABLE "public"."users" ADD UNIQUE ("email");
ALTER TABLE "public"."users" ADD UNIQUE ("telegram_alias");

-- ----------------------------
-- Primary Key structure for table "public"."users"
-- ----------------------------
ALTER TABLE "public"."users" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."group_bonus_users"
-- ----------------------------
ALTER TABLE "public"."group_bonus_users" ADD FOREIGN KEY ("group_bonus_id") REFERENCES "public"."group_bonuses" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."group_bonus_users" ADD FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."group_bonuses"
-- ----------------------------
ALTER TABLE "public"."group_bonuses" ADD FOREIGN KEY ("bonus_id") REFERENCES "public"."bonuses" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."group_bonuses" ADD FOREIGN KEY ("group_id") REFERENCES "public"."groups" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."group_users"
-- ----------------------------
ALTER TABLE "public"."group_users" ADD FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."group_users" ADD FOREIGN KEY ("group_id") REFERENCES "public"."groups" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
