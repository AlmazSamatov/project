## Requirements
To install requirements for our project run install_requirements.sh by typing: 
./install_requirements.sh It will install needed requirements, but if you will 
have some problems running it, then this is list of requirements:
1.  Java
2.  Maven
2.  Python
3.  NodeJS
4.  PostgreSQL
5.  Build-Essential

After this step, you need to create superuser in database with login/password: dds-project/dds-project.
You can do it in following way:
1.  sudo -u postgres psql
2.  sudo -u postgres createuser --interactive

And after you need to enter login and password.

## How to run our project

To run our project you need to do following steps:
1.  Copy our project by command: git clone https://gitlab.com/AlmazSamatov/project
2.  In /telegram-bot/telegram_bot.py change variable token in line 10 to one that you get from FatherBot
3.  After, in the same file, change server_address variable in line 11 to your server address
4.  Run sql script (dds-project-backend/db/db.sql) to create schema of database with admin user: admin@gmail.com:123123.
6.  Run restart.sh script by typing: ./restart.sh
7.  Also if you want to restart our project, run same script restart.sh

After swagger will run and have following url: (your server address):8080/swagger-ui.html#/ 
with login/password: swagger/swagger.
Frontend will run in port 3000 and have following url: (your server address):3000

