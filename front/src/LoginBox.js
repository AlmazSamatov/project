import React from 'react';
import './index.scss';
  
class LoginBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      psw: "",
    };
  }

  render() {
    return (
      <div className="inner-container">
        <div className="header">
          Login
        </div>
        <div className="box">
          <div className="input-group">
            <label htmlFor="email">Email</label>
            <input 
              type="text" 
              name="email" 
              className="login-input" 
              placeholder="Email"
              value={this.state.email}
              onChange={(e) => this.setState({email: e.target.value})}/>
          </div>
  
          <div className="input-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              name="password"
              className="login-input"
              placeholder="Password"
              value={this.state.psw}
              onChange={(e) => this.setState({psw: e.target.value})}/>
          </div>
  
          <button
            type="button"
            className="login-btn"
            onClick={_ => this.props.onSubmit(this.state.email, this.state.psw)}>
            Login
          </button>
        </div>
      </div>
    );
  }
}

export default LoginBox;