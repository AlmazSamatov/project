import React from 'react';
import './index.scss';

class RegisterBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      nameError: "",
      email: "",
      emailError: "",
      psw: "",
      pswError: "",
      cpsw: "",
      confirmError: ""
    };
    
    this.validateName = this.validateName.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
    this.validatePsw = this.validatePsw.bind(this);
    this.confirmPassword = this.confirmPassword.bind(this);
  }
  
  validateName(e){
    let name = e.target.value;
    this.setState({name});
    if(!name.match("^[a-zA-Z0-9]*$")){
      this.setState({nameError: "Name should contain only letters and digits."});
    } else {
      this.setState({nameError: ""});
    }
  }
  
  validateEmail(e){
    let email = e.target.value;
    this.setState({email});
    if(!email.match("^.+@.+$")){
      this.setState({emailError: "Invalid Email."});
    } else {
      this.setState({emailError: ""});
    }
  }
  
  validatePsw(e){
    let psw = e.target.value;
    if(psw !== "" && psw.length < 6){
      this.setState({pswError: "Password is too short."});
    } else {
      this.setState({pswError: ""});
    }
  }
  
  confirmPassword(psw, cpsw){
    if(psw !== cpsw){
      this.setState({confirmError: "Passwords do not match."});
    } else {
      this.setState({confirmError: ""});
    }
  }

  render() {
    return (
      <div className="inner-container">
        <div className="header">
          Register
        </div>
        <div className="box">
          <div className="input-group">
            <label htmlFor="text">Name</label>
            <input 
              type="text" 
              name="name" 
              className="login-input" 
              placeholder="Name"
              value={this.state.name}
              onChange={this.validateName}/>
          </div>
          {this.state.nameError !== "" && <div className="error-msg">
            {this.state.nameError}
          </div>}
          
          <div className="input-group">
            <label htmlFor="email">Email</label>
            <input 
              type="text" 
              name="email" 
              className="login-input" 
              placeholder="Email"
              value={this.state.email}
              onChange={this.validateEmail}/>
          </div>
          {this.state.emailError !== "" && <div className="error-msg">
            {this.state.emailError}
          </div>}
  
          <div className="input-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              name="password"
              className="login-input"
              placeholder="Password"
              value={this.state.psw}
              onChange={(e) => {
                this.validatePsw(e);
                this.setState({psw: e.target.value});
                this.confirmPassword(e.target.value, this.state.cpsw);
              }}/>
          </div>
          {this.state.pswError !== "" && <div className="error-msg">
            {this.state.pswError}
          </div>}
  
          <div className="input-group">
            <label htmlFor="password">Confirm Password</label>
            <input
              type="password"
              name="password"
              className="login-input"
              placeholder="Confirm Password"
              value={this.state.cpsw}
              onChange={(e) => {
                this.setState({cpsw: e.target.value});
                this.confirmPassword(this.state.psw, e.target.value);
              }}/>
          </div>
          {this.state.confirmError !== "" && <div className="error-msg">
            {this.state.confirmError}
          </div>}
          
          <button
            type="button"
            className="login-btn"
            onClick={_ => this.props.onSubmit(
              this.state.email, 
              this.state.name, 
              this.state.psw, 
              this.state.cpsw)}>
            Register
          </button>
        </div>
      </div>
    );
  }
}

export default RegisterBox;