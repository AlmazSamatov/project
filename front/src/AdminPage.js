import React from 'react';
import API from './API.js';
import Collapsible from 'react-collapsible';
import './index.scss';

class AdminPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        users: [],
        groups: [],
        bonuses: [],
        
        create_group: {
            id: "",
            title: "",
            description: "",
            is_visible: ""
        },
      
        show_user: {
          id: "",
          groups: []
        },
        
        manage_group: {
            id: "",
            users: [],
            user_id: ""
        },
        
        create_bonus: {
            id: "",
            title: "",
            description: "",
            isCountable: ""
        },
        
        manage_bonus: {
            bonus_id: "",
            group_id: "",
            amount: "",
            bonuses: []
        },
      
        manage_user_bonus: {
            id: "",
            user_id: "",
            amount: "",
            bonuses: []
        },
        
        errors: {
            user_groups: "",
            create_group: "",
            manage_group: "",
            manage_bonus: "",
            group_bonuses: "",
            user_bonuses: "",
        }
    };
  }
  
  componentDidMount() {
    this.props.update()
    API.get("/admin/users", {
      headers: {
        'Authorization': sessionStorage.authToken,
      }
    }).then(response => {
        let users = response.data.result.content;
        users.sort((a, b) => a.id - b.id);
        this.setState({users});
    }).catch(error => {
        console.log("AdminPageLoadError: " + error);
        this.props.toLoginState(error);
    });
    API.get("/admin/bonuses", {
      headers: {
        'Authorization': sessionStorage.authToken,
      }
    }).then(response => {
        let bonuses = response.data.result.content;
        bonuses.sort((a, b) => a.id - b.id);
        this.setState({bonuses});
    }).catch(error => {
        console.log("AdminPageLoadError: " + error);
        this.props.toLoginState(error);
    });
    API.get("/admin/groups", {
      headers: {
        'Authorization': sessionStorage.authToken,
      }
    }).then(response => {
        let groups = response.data.result.content;
        groups.sort((a, b) => a.id - b.id);
        this.setState({groups});
    }).catch(error => {
        console.log("AdminPageLoadError: " + error);
        this.props.toLoginState(error);
    });
  }
  
  showUserGroups() {
    let type = "user_groups"
    if(!this.state.show_user.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.get("/admin/users/" + this.state.show_user.id + "/groups", {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {
          let groups = response.data.result.content;
          groups.sort((a, b) => a.group.id - b.group.id);
          const { show_user } = this.state
          show_user.groups = groups
          this.setState({show_user});
          this.componentDidMount()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }

  createGroup() {
    let type = "create_group"
    if(!this.state.create_group.description.match("^[ a-zA-Z0-9]+$")){
      this.updateError(type, "Must contain only letters and digits.")
    } else if(!this.state.create_group.title.match("^[ a-zA-Z0-9]+$")){
      this.updateError(type, "Must contain only letters and digits.")
    } else if (this.state.create_group.is_visible !== 'true' && this.state.create_group.is_visible !== 'false'){
      this.updateError(type, "is_visible should be either 'true' or 'false'")
    } else {
      this.updateError(type, "")
      API.post("/admin/groups", {
          "description": this.state.create_group.description,
          "title": this.state.create_group.title,
          "is_visible": this.state.create_group.is_visible
        },
        {
          "headers":{
            'Authorization': sessionStorage.authToken,
          }
        }
      ).then(response => {this.componentDidMount()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  showGroup() {
    let type = "manage_group"
    if(!this.state.manage_group.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.get("/admin/groups/" + this.state.manage_group.id + "/users", {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {
          let users = response.data.result.content;
          users.sort((a, b) => a.user.id - b.user.id);
          const { manage_group } = this.state
          manage_group.users = users
          this.setState({manage_group});
          this.componentDidMount()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  deleteGroup() {
    let type = "manage_group"
    if(!this.state.manage_group.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.delete("/admin/groups/" + this.state.manage_group.id, {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {
          const { manage_group } = this.state
          if (manage_group.id === this.state.manage_group.id){
            manage_group.id = ""
            manage_group.users = []
            this.setState({manage_group});
          }
          this.componentDidMount()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  addUser() {
    let type = "manage_group"
    if(!this.state.manage_group.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_group.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.post("/admin/groups/" + this.state.manage_group.id + "/users/" + this.state.manage_group.user_id, {},
        {
          "headers":{
            'Authorization': sessionStorage.authToken,
          }
        }
      ).then(response => {this.showGroup()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  deleteUser() {
    let type = "manage_group"
    if(!this.state.manage_group.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_group.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.delete("/admin/groups/" + this.state.manage_group.id + "/users/" + this.state.manage_group.user_id,
        {
          "headers":{
            'Authorization': sessionStorage.authToken,
          }
        }
      ).then(response => {this.showGroup()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }

  addModer() {
    let type = "manage_group"
    if(!this.state.manage_group.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_group.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.post("/admin/groups/" + this.state.manage_group.id + "/users/" + this.state.manage_group.user_id + "/moderator", {},
        {
          "headers":{
            'Authorization': sessionStorage.authToken,
          }
        }
      ).then(response => {this.showGroup()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  deleteModer() {
    let type = "manage_group"
    if(!this.state.manage_group.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_group.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.delete("/admin/groups/" + this.state.manage_group.id + "/users/" + this.state.manage_group.user_id + "/moderator",
        {
          "headers":{
            'Authorization': sessionStorage.authToken,
          }
        }
      ).then(response => {this.showGroup()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  createBonus() {
    let type = "manage_bonus"
    if(!this.state.create_bonus.description.match("^[ a-zA-Z0-9]+$")){
      this.updateError(type, "Must contain only letters and digits.")
    } else if(!this.state.create_bonus.title.match("^[ a-zA-Z0-9]+$")){
      this.updateError(type, "Must contain only letters and digits.")
    } else if (this.state.create_bonus.isCountable !== 'true' && this.state.create_bonus.isCountable !== 'false'){
      this.updateError(type, "IsCountable should be either 'true' or 'false'")
    } else {
      this.updateError(type, "")
      API.post("/admin/bonuses", {
          "description": this.state.create_bonus.description,
          "title": this.state.create_bonus.title,
          "is_countable": (this.state.create_bonus.isCountable === 'true')
        },
        {
          "headers":{
            'Authorization': sessionStorage.authToken,
          }
        }
      ).then(response => {this.componentDidMount()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  deleteBonus() {
    let type = "manage_bonus"
    if(!this.state.create_bonus.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.delete("/admin/bonuses/" + this.state.create_bonus.id, {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {
          const { create_bonus } = this.state
          if (create_bonus.id === this.state.create_bonus.id){
            create_bonus.id = ""
            this.setState({create_bonus});
          }
          this.componentDidMount()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  showGroupBonuses() {
    let type = "group_bonuses"
    if(!this.state.manage_bonus.group_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.get("/admin/groups/" + this.state.manage_bonus.group_id + "/bonuses", {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {
          let bonuses = response.data.result.content;
          bonuses.sort((a, b) => a.group_bonus_id - b.group_bonus_id);
          const { manage_bonus } = this.state
          manage_bonus.bonuses = bonuses
          this.setState({manage_bonus});
          this.componentDidMount()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  addGroupBonus() {
    let type = "group_bonuses"
    if(!this.state.manage_bonus.group_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_bonus.bonus_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_bonus.amount.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.put("/admin/groups/" + this.state.manage_bonus.group_id + "/bonuses/" + this.state.manage_bonus.bonus_id, {
          "amount": this.state.manage_bonus.amount
        }, {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {this.showGroupBonuses()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  showUserBonuses() {
    let type = "user_bonuses"
    if(!this.state.manage_user_bonus.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.get("/admin/users/" + this.state.manage_user_bonus.user_id + "/bonuses", {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {
          let bonuses = response.data.result.content;
          bonuses.sort((a, b) => a.id - b.id);
          const { manage_user_bonus } = this.state
          manage_user_bonus.bonuses = bonuses
          this.setState({manage_user_bonus});
          this.componentDidMount()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  addUserBonus() {
    let type = "user_bonuses"
    if(!this.state.manage_user_bonus.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_user_bonus.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_user_bonus.amount.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.put("/admin/group/bonuses/" + this.state.manage_user_bonus.id + "/users/" + this.state.manage_user_bonus.user_id, {
          "amount": this.state.manage_user_bonus.amount
        }, {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {this.showUserBonuses()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  deleteUserBonus() {
    let type = "user_bonuses"
    if(!this.state.manage_user_bonus.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_user_bonus.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_user_bonus.amount.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.delete("/admin/group/bonuses/" + this.state.manage_user_bonus.id + "/users/" + this.state.manage_user_bonus.user_id,{
        headers: {
          'Authorization': sessionStorage.authToken,
        }, data: {
          "amount": this.state.manage_user_bonus.amount
        }
      }).then(response => {this.showUserBonuses()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }

  getUserGroups() {
    return this.state.show_user.groups.map(group => (
      <tr key={group.group['id']}>
        <td>{group.group['id']}</td>
        <td>{group.group['title']}</td>
        <td>{group.role}</td>
      </tr>
    ));
  }
  
  getGroupUsers() {
    return this.state.manage_group.users.map(user => (
      <tr key={user.user['id']}>
        <td>{user.user['id']}</td>
        <td>{user.user['email'] != null ? user.user['email'] : user.user['telegram_alias']}</td>
        <td>{user.user['name']}</td>
        <td>{user.role}</td>
      </tr>
    ));
  }
  
  getUsers(){
    return this.state.users.map(user => (
      <tr key={user['id']}>
        <td>{user['id']}</td>
        <td>{user['email'] != null ? user['email'] : user['telegram_alias']}</td>
        <td>{user['name']}</td>
        <td>{user['role']}</td>
      </tr>
    ));
  }
  
  getBonuses(){
    return this.state.bonuses.map(bonus => (
      <tr key={bonus['id']}>
        <td>{bonus['id']}</td>
        <td>{bonus['title']}</td>
        <td>{bonus['description']}</td>
        <td>{bonus['isCountable'] ? "true" : "false"}</td>
      </tr>
    ));
  }
  
  getGroups(){
    return this.state.groups.map(group => (
      <tr key={group['id']}>
        <td>{group['id']}</td>
        <td>{group['title']}</td>
        <td>{group['description']}</td>
        <td>{group['invitation_code']}</td>
        <td>{group['is_visible'] ? "true" : "false"}</td>
      </tr>
    ));
  }

  getGroupBonuses(){
    return this.state.manage_bonus.bonuses.map(bonus => (
      <tr key={bonus['group_bonus_id']}>
        <td>{bonus['group_bonus_id']}</td>
        <td>{bonus.bonus['id']}</td>
        <td>{bonus.bonus['title']}</td>
        <td>{bonus.bonus['isCountable'] ? bonus.amount : "-"}</td>
      </tr>
    ));
  }
  
  getUserBonuses(){
    return this.state.manage_user_bonus.bonuses.map(bonus => (
      <tr key={bonus['group_bonus_id']}>
        <td>{bonus['group_bonus_id']}</td>
        <td>{bonus.bonus['id']}</td>
        <td>{bonus.group['id']}</td>
        <td>{bonus.bonus['title']}</td>
        <td>{bonus.bonus['isCountable'] ? bonus.amount : "-"}</td>
      </tr>
    ));
  }
  
  updateError(type, error){
      const { errors } = this.state;
      errors[type] = error;
      this.setState({ errors })
  }
  
  showRequestError(type, error){
      let data = error;
      if (data != null){
        data = data.response
      }
      if (data != null){
        data = data.data
      }
      if (data != null){
        data = data.error
      }
      
      if (data == null) {
        this.updateError(type, error.message)
      } else if (data.details != null){
        this.updateError(type, data.details[0].message)
      } else if (data.message != null){
        this.updateError(type, data.message)
      } else {
        this.updateError(type, data)
      }
  }
  
  updateState(type, key, value){
      const obj = this.state[type];
      obj[key] = value;
      this.setState({ type: obj })
  }
  
  render() {
    return (
      <div className="inner-container">
        <Collapsible 
            trigger="All Users" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>Id</th>
                  <th>Email</th>
                  <th>Name</th>
                  <th>Role</th>
                </tr>
                {this.getUsers()}
              </tbody>
            </table>
          </div>
        </Collapsible>

        <Collapsible
            trigger="All Groups"
            triggerTagName="div"
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Invitation code</th>
                  <th>Visible</th>
                </tr>
                {this.getGroups()}
              </tbody>
            </table>
          </div>
        </Collapsible>

        <Collapsible 
            trigger="Show User Groups" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="form">
            <input 
              className="login-input" 
              placeholder="User id"
              value={this.state.show_user.id}
              onChange={(e) => this.updateState("show_user", "id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.showUserGroups()}>
              Show
            </button>
          </div>
          {this.state.errors.user_groups !== "" && <div className="big-error-msg"> 
            {this.state.errors.user_groups} </div>}
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>GroupRole</th>
                </tr>
                {this.getUserGroups()}
              </tbody>
            </table>
          </div>
        </Collapsible>

        <Collapsible 
            trigger="Create Group" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="form">
            <input
              className="login-input" 
              placeholder="Group Title"
              value={this.state.create_group.title}
              onChange={(e) => this.updateState("create_group", "title", e.target.value)}/>
          </div>
          <div className="form">
            <input
              className="login-input" 
              placeholder="Group Description"
              value={this.state.create_group.description}
              onChange={(e) => this.updateState("create_group", "description", e.target.value)}/>
          </div>
          <div className="form">
            <input
              className="login-input" 
              placeholder="Group Visible"
              value={this.state.create_group.is_visible}
              onChange={(e) => this.updateState("create_group", "is_visible", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={() => this.createGroup()}>
              Create
            </button>
          </div>
          {this.state.errors.create_group !== "" && <div className="big-error-msg"> 
            {this.state.errors.create_group} </div>}
        </Collapsible>
         
        <Collapsible 
            trigger="Manage Group" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Group id"
              value={this.state.manage_group.id}
              onChange={(e) => this.updateState("manage_group", "id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.showGroup()}>
              Show
            </button>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.deleteGroup()}>
              Delete
            </button>
          </div>
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Add or delete user by id"
              value={this.state.manage_group.user_id}
              onChange={(e) => this.updateState("manage_group", "user_id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.addUser()}>
              Add
            </button>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.deleteUser()}>
              Delete
            </button>
          </div>
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Set or unset moder by user id"
              value={this.state.manage_group.user_id}
              onChange={(e) => this.updateState("manage_group", "user_id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.addModer()}>
              Set
            </button>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.deleteModer()}>
              Unset
            </button>
          </div>
          {this.state.errors.manage_group !== "" && <div className="big-error-msg"> 
            {this.state.errors.manage_group} </div>}
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>Id</th>
                  <th>Email</th>
                  <th>Name</th>
                  <th>GroupRole</th>
                </tr>
                {this.getGroupUsers()}
              </tbody>
            </table>
          </div>
        </Collapsible>
        
        <Collapsible 
            trigger="All Bonuses" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Countable</th>
                </tr>
                {this.getBonuses()}
              </tbody>
            </table>
          </div>
        </Collapsible>

        <Collapsible 
            trigger="Manage Bonus" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="form">
            <input
              className="login-input" 
              placeholder="Bonus Title"
              value={this.state.create_bonus.title}
              onChange={(e) => this.updateState("create_bonus", "title", e.target.value)}/>
          </div>
          <div className="form">
            <input
              className="login-input" 
              placeholder="Bonus Description"
              value={this.state.create_bonus.description}
              onChange={(e) => this.updateState("create_bonus", "description", e.target.value)}/>
          </div>
          <div className="form">
            <input
              className="login-input" 
              placeholder="Is Countable"
              value={this.state.create_bonus.isCountable}
              onChange={(e) => {this.updateState("create_bonus", "isCountable", e.target.value)}}/>
            <button
              type="button"
              className="form-btn"
              onClick={() => this.createBonus()}>
              Create
            </button>
          </div>
          <div className="form">
            <input
              className="login-input" 
              placeholder="Bonus Id to delete"
              value={this.state.create_bonus.id}
              onChange={(e) => this.updateState("create_bonus", "id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={() => this.deleteBonus()}>
              Delete
            </button>
          </div>
          {this.state.errors.manage_bonus !== "" && <div className="big-error-msg"> 
            {this.state.errors.manage_bonus} </div>}
        </Collapsible>
        
        <Collapsible 
            trigger="Manage Group Bonuses" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Group id"
              value={this.state.manage_bonus.group_id}
              onChange={(e) => this.updateState("manage_bonus", "group_id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.showGroupBonuses()}>
              Show
            </button>
          </div>
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Bonus amount"
              value={this.state.manage_bonus.amount}
              onChange={(e) => this.updateState("manage_bonus", "amount", e.target.value)}/>
          </div>
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Add bonus by id"
              value={this.state.manage_bonus.bonus_id}
              onChange={(e) => this.updateState("manage_bonus", "bonus_id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.addGroupBonus()}>
              Add
            </button>
          </div>
          {this.state.errors.group_bonuses !== "" && <div className="big-error-msg"> 
            {this.state.errors.group_bonuses} </div>}
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>GBId</th>
                  <th>Bonus Id</th>
                  <th>Title</th>
                  <th>Amount</th>
                </tr>
                {this.getGroupBonuses()}
              </tbody>
            </table>
          </div>
        </Collapsible>
        
        <Collapsible 
            trigger="Manage User Bonuses" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="form">
            <input 
              className="login-input" 
              placeholder="User id"
              value={this.state.manage_user_bonus.user_id}
              onChange={(e) => this.updateState("manage_user_bonus", "user_id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.showUserBonuses()}>
              Show
            </button>
          </div>
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Bonus amount"
              value={this.state.manage_user_bonus.amount}
              onChange={(e) => this.updateState("manage_user_bonus", "amount", e.target.value)}/>
          </div>
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Group bonus id"
              value={this.state.manage_user_bonus.id}
              onChange={(e) => this.updateState("manage_user_bonus", "id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.addUserBonus()}>
              Add
            </button>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.deleteUserBonus()}>
              Delete
            </button>
          </div>
          {this.state.errors.user_bonuses !== "" && <div className="big-error-msg"> 
            {this.state.errors.user_bonuses} </div>}
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>GBId</th>
                  <th>Bonus Id</th>
                  <th>Group Id</th>
                  <th>Title</th>
                  <th>Amount</th>
                </tr>
                {this.getUserBonuses()}
              </tbody>
            </table>
          </div>
        </Collapsible>
      </div>
    );
  }
}

export default AdminPage;