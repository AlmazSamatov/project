import axios from "axios";

const API = axios.create({
  baseURL: "http://134.209.252.11:8080/v1/",
  responseType: "json"
});

API.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
API.defaults.headers.post['Content-Type'] = 'application/json';

export default API;