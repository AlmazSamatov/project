import React from 'react';
import API from './API.js';
import Collapsible from 'react-collapsible';

import AdminPage from './AdminPage.js';
import './index.scss';

class UserPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        id: "",
        email: "",
        name: "",
        role: "",
        groups: [],
        moder_groups: [],
        user_groups: [],
        user_bonuses: [],
        group_id: "",
        group_bonus_id: "",
        amount: "",
      
        manage_group: {
            id: "",
            users: [],
            user_id: "",
        },
        
        manage_bonus: {
            bonus_id: "",
            group_id: "",
            amount: "",
            bonuses: [],
        },
      
        manage_user_bonus: {
            id: "",
            user_id: "",
            amount: "",
            bonuses: [],
        },
        
        errors: {
            my_groups: "",
            my_bonuses: "",
            moder_group: "",
            moder_group_bonuses: "",
            moder_user_bonuses: "",
        }
    };
  }
  
  componentDidMount() {
    API.get("/user", {
      headers: {
        'Authorization': sessionStorage.authToken,
      }
    }).then(response => {
        let user = response.data.result;
        this.setState({
            id: user.id,
            email: user.email ? user.email : user.telegram_alias,
            name: user.name,
            role: user.role
        });
    }).catch(error => {
        console.log("UserPageLoadError: " + error);
        this.props.toLoginState(error);
    });
    API.get("/groups", {
      headers: {
        'Authorization': sessionStorage.authToken,
      }
    }).then(response => {
        let groups = response.data.result.content;
        groups.sort((a, b) => a.id - b.id);
        this.setState({groups});
    }).catch(error => {
        console.log("UserPageLoadError: " + error);
        this.props.toLoginState(error);
    });
    this.showGroups()
    this.showModerGroups()
    this.showBonuses()
  }
  
  showGroups() {
    API.get("/user/groups/extended", {
      headers: {
        'Authorization': sessionStorage.authToken,
      }
    }).then(response => {
        let user_groups = response.data.result.content;
        user_groups.sort((a, b) => a.group.id - b.group.id);
        this.setState({user_groups});
    }).catch(error => {
        console.log("showGroupsError: " + error);
    });
  }
  
  showModerGroups() {
    API.get("/moderator/groups", {
      headers: {
        'Authorization': sessionStorage.authToken,
      }
    }).then(response => {
        let moder_groups = response.data.result.content;
        moder_groups.sort((a, b) => a.id - b.id);
        this.setState({moder_groups});
    }).catch(error => {
        console.log("showModerGroupsError: " + error);
    });
  }

  showBonuses() {
    API.get("/user/bonuses", {
      headers: {
        'Authorization': sessionStorage.authToken,
      }
    }).then(response => {
        let user_bonuses = response.data.result.content;
        user_bonuses.sort((a, b) => a.group_bonus_id - b.group_bonus_id);
        this.setState({user_bonuses});
    }).catch(error => {
        console.log("showBonusesError: " + error);
    });
  }

  applyGroup() {
    let type = "my_groups"
    if(!this.state.group_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.post("/user/groups", {group_id: this.state.group_id},
        {
          "headers":{
            'Authorization': sessionStorage.authToken,
          }
        }
      ).then(response => {this.showGroups()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  deleteGroup() {
    let type = "my_groups"
    if(!this.state.group_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      API.delete("/user/groups/" + this.state.group_id,
        {
          "headers":{
            'Authorization': sessionStorage.authToken,
          }
        }
      ).then(response => {this.showGroups()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  deleteBonus() {
    let type = "my_bonuses"
    if(!this.state.group_bonus_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.amount.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.delete("/user/bonuses/" + this.state.group_bonus_id, {
        headers: {
          'Authorization': sessionStorage.authToken,
        }, data: {
          "amount": this.state.amount
        }
       }
      ).then(response => {this.showBonuses()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  showGroup() {
    let type = "moder_group"
    if(!this.state.manage_group.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.get("/moderator/groups/" + this.state.manage_group.id + "/users", {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {
          let users = response.data.result.content;
          users.sort((a, b) => a.user.id - b.user.id);
          const { manage_group } = this.state
          manage_group.users = users
          this.setState({manage_group});
          this.componentDidMount()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  addUser() {
    let type = "moder_group"
    if(!this.state.manage_group.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_group.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.post("/moderator/groups/" + this.state.manage_group.id + "/users/" + this.state.manage_group.user_id, {},
        {
          "headers":{
            'Authorization': sessionStorage.authToken,
          }
        }
      ).then(response => {this.showGroup()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  deleteUser() {
    let type = "moder_group"
    if(!this.state.manage_group.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_group.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.delete("/moderator/groups/" + this.state.manage_group.id + "/users/" + this.state.manage_group.user_id,
        {
          "headers":{
            'Authorization': sessionStorage.authToken,
          }
        }
      ).then(response => {this.showGroup()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  showGroupBonuses() {
    let type = "moder_group_bonuses"
    if(!this.state.manage_bonus.group_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.get("/moderator/groups/" + this.state.manage_bonus.group_id + "/bonuses", {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {
          let bonuses = response.data.result.content;
          bonuses.sort((a, b) => a.group_bonus_id - b.group_bonus_id);
          const { manage_bonus } = this.state
          manage_bonus.bonuses = bonuses
          this.setState({manage_bonus});
          this.componentDidMount()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  showUserBonuses() {
    let type = "moder_user_bonuses"
    if(!this.state.manage_user_bonus.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_user_bonus.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.get("/moderator/group/bonuses/" + this.state.manage_user_bonus.id + "/users/" + 
              this.state.manage_user_bonus.user_id, {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {
          let bonus = response.data.result;
          const { manage_user_bonus } = this.state
          manage_user_bonus.bonuses = [bonus]
          manage_user_bonus.error = ""
          this.setState({manage_user_bonus});
          this.componentDidMount()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  addUserBonus() {
    let type = "moder_user_bonuses"
    if(!this.state.manage_user_bonus.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_user_bonus.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_user_bonus.amount.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.put("/moderator/group/bonuses/" + this.state.manage_user_bonus.id + "/users/" + 
              this.state.manage_user_bonus.user_id, {
          "amount": this.state.manage_user_bonus.amount
        }, {
        headers: {
          'Authorization': sessionStorage.authToken,
        }
      }).then(response => {this.showUserBonuses()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }
  
  deleteUserBonus() {
    let type = "moder_user_bonuses"
    if(!this.state.manage_user_bonus.id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_user_bonus.user_id.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else if(!this.state.manage_user_bonus.amount.match("^[0-9]+$")){
      this.updateError(type, "Must be int")
    } else {
      this.updateError(type, "")
      API.delete("/moderator/group/bonuses/" + this.state.manage_user_bonus.id + "/users/" + 
                 this.state.manage_user_bonus.user_id,{
        headers: {
          'Authorization': sessionStorage.authToken,
        }, data: {
          "amount": this.state.manage_user_bonus.amount
        }
      }).then(response => {this.showUserBonuses()
      }).catch(error => {this.showRequestError(type, error)});
    }
  }

  getGroups(){
    return this.state.groups.map(group => (
      <tr key={group['id']}>
        <td>{group['id']}</td>
        <td>{group['title']}</td>
        <td>{group['description']}</td>
      </tr>
    ));
  }

  getModerGroups(){
    return this.state.moder_groups.map(group => (
      <tr key={group['id']}>
        <td>{group['id']}</td>
        <td>{group['title']}</td>
        <td>{group['description']}</td>
        <td>{group['invitation_code']}</td>
        <td>{group['is_visible'] ? "true" : "false"}</td>
      </tr>
    ));
  }

  getUserGroups() {
    return this.state.user_groups.map(group => (
      <tr key={group.group['id']}>
        <td>{group.group['id']}</td>
        <td>{group.group['title']}</td>
        <td>{group.role}</td>
      </tr>
    ));
  }
  
  getUserBonuses() {
    return this.state.user_bonuses.map(bonus => (
      <tr key={bonus['group_bonus_id']}>
        <td>{bonus['group_bonus_id']}</td>
        <td>{bonus.bonus['id']}</td>
        <td>{bonus.group['id']}</td>
        <td>{bonus.bonus['title']}</td>
        <td>{bonus.bonus['isCountable'] ? bonus.amount : "-"}</td>
      </tr>
    ));
  }
  
  getGroupBonuses(){
    return this.state.manage_bonus.bonuses.map(bonus => (
      <tr key={bonus['group_bonus_id']}>
        <td>{bonus['group_bonus_id']}</td>
        <td>{bonus.bonus['id']}</td>
        <td>{bonus.bonus['title']}</td>
        <td>{bonus.bonus['isCountable'] ? bonus.amount : "-"}</td>
      </tr>
    ));
  }
  
  getGroupUsers() {
    return this.state.manage_group.users.map(user => (
      <tr key={user.user['id']}>
        <td>{user.user['id']}</td>
        <td>{user.user['email'] != null ? user.user['email'] : user.user['telegram_alias']}</td>
        <td>{user.user['name']}</td>
        <td>{user.role}</td>
      </tr>
    ));
  }
  
  getModerUserBonuses(){
    return this.state.manage_user_bonus.bonuses.map(bonus => (
      <tr key={bonus['group_bonus_id']}>
        <td>{bonus['group_bonus_id']}</td>
        <td>{bonus.bonus['id']}</td>
        <td>{bonus.group['id']}</td>
        <td>{bonus.bonus['title']}</td>
        <td>{bonus.bonus['isCountable'] ? bonus.amount : "-"}</td>
      </tr>
    ));
  }
  
  updateError(type, error){
      const { errors } = this.state;
      errors[type] = error;
      this.setState({ errors })
  }
  
  showRequestError(type, error){
      let data = error;
      if (data != null){
        data = data.response
      }
      if (data != null){
        data = data.data
      }
      if (data != null){
        data = data.error
      }
      if (data == null) {
        this.updateError(type, error.message)
      } else if (data.details != null){
        this.updateError(type, data.details[0].message)
      } else if (data.message != null){
        this.updateError(type, data.message)
      } else {
        this.updateError(type, data)
      }
  }
  
  updateState(type, key, value){
      const obj = this.state[type];
      obj[key] = value;
      this.setState({ type: obj })
  }
  
  render() {
    return (
      <div>
        <Collapsible 
            trigger="User Page" 
            triggerTagName="div" 
            triggerClassName="userPageHeader"
            triggerOpenedClassName="userPageHeader"
            contentInnerClassName="openedCollapsible"
            open={true}>
          <div className="box">
            <table id="dynamic" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <td>Id</td>
                  <td>{this.state.id}</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>{this.state.email}</td>
                </tr>
                <tr>
                  <td>Name</td>
                  <td>{this.state.name}</td>
                </tr>
                <tr>
                  <td>Role</td>
                  <td>{this.state.role}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </Collapsible>

        <Collapsible 
            trigger="Groups" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Description</th>
                </tr>
                {this.getGroups()}
              </tbody>
            </table>
          </div>
        </Collapsible>

        <Collapsible 
            trigger="My Groups" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Group id"
              value={this.state.group_id}
              onChange={(e) => this.setState({ group_id: e.target.value })}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.applyGroup()}>
              Apply
            </button>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.deleteGroup()}>
              Leave
            </button>
          </div>
          {this.state.errors.my_groups !== "" && <div className="big-error-msg"> 
            {this.state.errors.my_groups} </div>}
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>GroupRole</th>
                </tr>
                {this.getUserGroups()}
              </tbody>
            </table>
          </div>
        </Collapsible>

        <Collapsible 
            trigger="My Bonuses" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Bonus amount"
              value={this.state.amount}
              onChange={(e) => this.setState({ amount: e.target.value })}/>
          </div>
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Group Bonus id"
              value={this.state.group_bonus_id}
              onChange={(e) => this.setState({ group_bonus_id: e.target.value })}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.deleteBonus()}>
              Delete
            </button>
          </div>
          {this.state.errors.my_bonuses !== "" && <div className="big-error-msg"> 
            {this.state.errors.my_bonuses} </div>}
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>GBId</th>
                  <th>Bonus Id</th>
                  <th>Group Id</th>
                  <th>Title</th>
                  <th>Amount</th>
                </tr>
                {this.getUserBonuses()}
              </tbody>
            </table>
          </div>
        </Collapsible>

        <Collapsible 
            trigger="Moderate Groups" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Invitation Code</th>
                  <th>Visible</th>
                </tr>
                {this.getModerGroups()}
              </tbody>
            </table>
          </div>
        </Collapsible>

        <Collapsible 
            trigger="Moderate Group" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Group id"
              value={this.state.manage_group.id}
              onChange={(e) => this.updateState("manage_group", "id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.showGroup()}>
              Show
            </button>
          </div>
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Add or delete user by id"
              value={this.state.manage_group.user_id}
              onChange={(e) => this.updateState("manage_group", "user_id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.addUser()}>
              Add
            </button>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.deleteUser()}>
              Delete
            </button>
          </div>
          {this.state.errors.moder_group !== "" && <div className="big-error-msg"> 
            {this.state.errors.moder_group} </div>}
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>Id</th>
                  <th>Email</th>
                  <th>Name</th>
                  <th>GroupRole</th>
                </tr>
                {this.getGroupUsers()}
              </tbody>
            </table>
          </div>
        </Collapsible>
        
        <Collapsible 
            trigger="Moderate Group Bonuses" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Group id"
              value={this.state.manage_bonus.group_id}
              onChange={(e) => this.updateState("manage_bonus", "group_id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.showGroupBonuses()}>
              Show
            </button>
          </div>
          {this.state.errors.moder_group_bonuses !== "" && <div className="big-error-msg"> 
            {this.state.errors.moder_group_bonuses} </div>}
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>GBId</th>
                  <th>Bonus Id</th>
                  <th>Title</th>
                  <th>Amount</th>
                </tr>
                {this.getGroupBonuses()}
              </tbody>
            </table>
          </div>
        </Collapsible>
        
        <Collapsible 
            trigger="Moderate User Bonuses" 
            triggerTagName="div" 
            triggerClassName="header"
            triggerOpenedClassName="header"
            contentInnerClassName="openedCollapsible">
          <div className="form">
            <input 
              className="login-input" 
              placeholder="User id"
              value={this.state.manage_user_bonus.user_id}
              onChange={(e) => this.updateState("manage_user_bonus", "user_id", e.target.value)}/>
          </div>
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Bonus amount"
              value={this.state.manage_user_bonus.amount}
              onChange={(e) => this.updateState("manage_user_bonus", "amount", e.target.value)}/>
          </div>
          <div className="form">
            <input 
              className="login-input" 
              placeholder="Group bonus id"
              value={this.state.manage_user_bonus.id}
              onChange={(e) => this.updateState("manage_user_bonus", "id", e.target.value)}/>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.showUserBonuses()}>
              Show
            </button>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.addUserBonus()}>
              Add
            </button>
            <button
              type="button"
              className="form-btn"
              onClick={_ => this.deleteUserBonus()}>
              Delete
            </button>
          </div>
          {this.state.errors.moder_user_bonuses !== "" && <div className="big-error-msg"> 
            {this.state.errors.moder_user_bonuses} </div>}
          <div className="box">
            <table className="table" cellSpacing="0" cellPadding="5">
              <tbody>
                <tr>
                  <th>GBId</th>
                  <th>Bonus Id</th>
                  <th>Group Id</th>
                  <th>Title</th>
                  <th>Amount</th>
                </tr>
                {this.getModerUserBonuses()}
              </tbody>
            </table>
          </div>
        </Collapsible>

        {this.state.role === "ADMIN" && 
          <AdminPage update={this.componentDidMount.bind(this)} toLoginState={this.props.toLoginState}/>}

        <div className="some-container">
          <button
            type="button"
            className="logout-btn"
            onClick={_ => this.props.toLoginState()}>
            Logout
          </button>
        </div>
      </div>
    );
  }
}

export default UserPage;