import React from 'react';
import API from './API.js';

import LoginBox from './LoginBox.js';
import RegisterBox from './RegisterBox.js';
import UserPage from './UserPage.js';
import './index.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      state: "login",
      error: ""
    };
    
    this.showLoginBox = this.showLoginBox.bind(this)
    this.showRegisterBox = this.showRegisterBox.bind(this)
    this.showUserPage = this.showUserPage.bind(this)
    
    this.submitLogin = this.submitLogin.bind(this)
    this.submitRegister = this.submitRegister.bind(this)
  }
  
  componentDidMount(){
    var token = sessionStorage.authToken;
    if (token !== 'null' && token){
      this.setState({state: "user"});
    } else {
      this.setState({state: "login"});
    }
  }

  showLoginBox(error="") {
      sessionStorage.authToken = null;
      this.setState({state: "login", error});
  }

  showRegisterBox() {this.setState({state: "register"});}

  showUserPage() {this.setState({state: "user"});}

  submitLogin(email, password) {    
    API.post("/users/login", { 
      email,
      password
    }).then(response => {
        sessionStorage.authToken = response.data.result.auth_token;
        this.setState({error: ""});
        this.showUserPage();
      }).catch(error => {this.showRequestError(error)});
  }
  
  submitRegister(email, name, psw, cpsw) {    
    API.post("/users/registration", { 
      email,
      name,
      password: psw,
      confirm_password: cpsw
    }).then(response => {
        this.setState({error: ""});
        this.submitLogin(email, psw);
      }).catch(error => {this.showRequestError(error)});
  }
  
  showRequestError(error){
      let data = error;
      if (data != null){
        data = data.response
      }
      if (data != null){
        data = data.data
      }
      if (data != null){
        data = data.error
      }
      if (data == null) {
        this.setState({ error: error.message})
      } else if (data.details != null){
        this.setState({ error: data.details[0].message})
      } else if (data.message != null){
        this.setState({ error: data.message})
      } else {
        this.setState({ error: data})
      }
  }

  render() {
    return (
      <div className="root-container">
       <div className="box-container">
        {this.state.state === "login" && 
          <LoginBox onSubmit={this.submitLogin}/>}
        {this.state.state === "register" && 
          <RegisterBox onSubmit={this.submitRegister}/>}
        {this.state.state === "user" && 
          <UserPage toLoginState={this.showLoginBox}/>}
        {this.state.error !== "" && 
          <div className="big-error-msg">
            {this.state.error}
          </div>}
       </div>
       
       {(this.state.state === "login" || 
         this.state.state === "register") && 
         <div className="box-controller">
           <div
             className={"controller " + (
               this.state.state === "login"
               ? "selected-controller" : "")}
             onClick={_ => this.showLoginBox()}>
             Login
           </div>
           <div
             className={"controller " + (
               this.state.state === "register"
               ? "selected-controller" : "")}
             onClick={_ => this.showRegisterBox()}>
             Register
           </div>
         </div>
       }
      </div>
    );
  }
}

export default App;
