sudo apt-get update

sudo apt-get install -y build-essential
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo apt-get install openjdk-8-jre
sudo apt-get install openjdk-8-jdk
sudo vi /etc/profile
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre"
source /etc/profile
sudo apt-get -y install maven

sudo apt-get install python3

sudo apt install postgresql postgresql-contrib